var Crowdsale = artifacts.require("PreIco")
// get also PhiToken
var PhiToken = artifacts.require("PhiToken")
// get RC
var RC = artifacts.require("ReservationContract")

// use bn library to convert and do math operations on big numbers
const bn = require('bignumber.js')

const abiDecoder = require('abi-decoder')
abiDecoder.addABI(Crowdsale.abi)

// testing data
var walletAddr = null
var ownerAddr = null
const maxTokenToBeSold = 3524578000000000000000000 // tokens to be sold on pre-ico
const phiRatePreIco = 1278246852100000000 // pre-ico fixed price (1,61803399 * 21%) * 10**18
const startTime = parseInt(Date.now()/1000) + 30 // start in 30 secs
const endTime = startTime + 30 // end after 30 secs
const intervalUpdate = 60 // oraclize updates in seconds
var instance = null
var PhiTokenInstance = null

var investorBalanceTest1 = null

/**
 * Returns a random integer between min (inclusive) and max (inclusive)
 * Using Math.round() will give you a non-uniform distribution!
 */
function getRandomInt(min, max) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
}

// timeout helper function
function timeout(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
}
// log helper
function log(txt) {
  console.log(`    ⬐ ${txt}`)
}

contract('Pre-ico basic tests w/ 10 investor (only 7 whitelisted)', function(accounts) {
  before(async function() {
    walletAddr = accounts[1]
    ownerAddr = accounts[2]
    // deploy new instances for the tests
    instance = await Crowdsale.new(walletAddr, {from: ownerAddr})
    PhiTokenInstance = await PhiToken.new(accounts[4], instance.address, walletAddr, parseInt(Date.now()/1000) + 600, {from: ownerAddr})
  });

  it('verify crowdsale deploy state', async () => {
    const owner = await instance.owner.call();
    assert.equal(owner, ownerAddr);

    const wallet = await instance.wallet.call();
    assert.equal(wallet, walletAddr);

    // check stage
    const nowStage = await instance.currentStage.call();
    // should be 0 = ToInitialize, // Crowdsale has not been initialized
    assert.strictEqual(nowStage.toNumber(), 0);

    const MAX_TOKENS = await instance.MAX_TOKENS.call();
    assert.strictEqual(MAX_TOKENS.toNumber(), maxTokenToBeSold);

    const phiRate = await instance.phiRate.call();
    assert.strictEqual(phiRate.toNumber(), phiRatePreIco);
  });

  it('should fail init crowdsale with wrong token address', function() {
    return instance.initializeCrowdsale(startTime, endTime, accounts[3], intervalUpdate, {from: ownerAddr, value: 1e8})
    .then(assert.fail)
    .catch((e) => {
      // should throw
      assert.isDefined(e)
      assert.match(e, /revert/)
    })
  });

  it('should fail init crowdsale with wrong timestamps', function() {
    // here startTime is > of endTime
    return instance.initializeCrowdsale(endTime, startTime, PhiTokenInstance.address, intervalUpdate, {from: ownerAddr, value: 1e8})
    .then(assert.fail)
    .catch((e) => {
      // should throw
      assert.isDefined(e)
      assert.match(e, /revert/)
    })
  });

  it('should fail init crowdsale with no ETH value', function() {
    // here startTime is > of endTime
    return instance.initializeCrowdsale(startTime, endTime, PhiTokenInstance.address, intervalUpdate, {from: ownerAddr, value: 0})
    .then(assert.fail)
    .catch((e) => {
      // should throw
      assert.isDefined(e)
      assert.match(e, /revert/)
    })
  });

  it('should fail init crowdsale from not an owner', function() {
    // here startTime is > of endTime
    return instance.initializeCrowdsale(startTime, endTime, PhiTokenInstance.address, intervalUpdate, {from: accounts[6], value: 1e8})
    .then(assert.fail)
    .catch((e) => {
      // should throw
      assert.isDefined(e)
      assert.match(e, /revert/)
    })
  });

  it('should fail init crowdsale with interval update too low', function() {
    // here startTime is > of endTime
    return instance.initializeCrowdsale(startTime, endTime, PhiTokenInstance.address, 5, {from: accounts[6], value: 1e8})
    .then(assert.fail)
    .catch((e) => {
      // should throw
      assert.isDefined(e)
      assert.match(e, /revert/)
    })
  });

  it('direct investment to pre-ico should fail', function() {
    return instance.sendTransaction({from: accounts[7], value: web3.toWei(1, 'ether')})
    .then(assert.fail)
    .catch((e) => {
      // should throw
      assert.isDefined(e)
      assert.match(e, /revert/)
    })
  });

  it('should init crowdsale (succeed)', function() {
    return instance.initializeCrowdsale(startTime, endTime, PhiTokenInstance.address, intervalUpdate, {from: ownerAddr, value: 1e8})
    .then((tx) => {
      assert.isNotNull(tx)
      log(`gas cost: ${tx.receipt.cumulativeGasUsed} wei`)
      // check if init event is fired
      assert.equal(tx.logs[0].event, 'LogCrowdsaleInit')
      // check if Oraclize event is fired (oraclize_query)
      assert.equal(tx.receipt.logs[0].topics[0], '0xb76d0edd90c6a07aa3ff7a222d7f5933e29c6acc660c059c97837f05c4ca1a84')
    })
  });

  it('should add a reservation contract', async () => {
      // deploy rc with the pre-ico address
      RcInstance = await RC.new(instance.address, {from: ownerAddr})
      const rcPreIcoContract = await RcInstance.preIcoAddr.call()
      assert.equal(rcPreIcoContract.toString(), instance.address)
      const startRc = await RcInstance.preIcoStart.call()
      const endRc = await RcInstance.preIcoEnd.call()
      assert.equal(startRc.toNumber(), startTime)
      assert.equal(endRc.toNumber(), endTime)

      const tx = await instance.addNewReservContract(RcInstance.address, {from: ownerAddr})
      log(`gas cost: ${tx.receipt.cumulativeGasUsed} wei`)
      // check if rc was added
      var isRcRecord = await instance.reservationContracts.call(RcInstance.address)
      assert.equal(isRcRecord, true)
  });

  it('investment should fail if crowdsale is not running (startTime)', function() {
    return RcInstance.sendTransaction({from: accounts[7], value: web3.toWei(1, 'ether')})
    .then(assert.fail)
    .catch((e) => {
      // should throw
      assert.isDefined(e)
      assert.match(e, /revert/)
    })
  });

  it('investment directly to pre-ico should fail before startTime', function() {
    return instance.sendTransaction({from: accounts[7], value: web3.toWei(1, 'ether')})
    .then(assert.fail)
    .catch((e) => {
      // should throw
      assert.isDefined(e)
      assert.match(e, /revert/)
    })
  });

  it('check ETH/USD rate and PHI rate before the official startTime', async () => {
    // wait 30 seconds
    await timeout(30*1000)
    // wait 15 seconds because of the ethereum-bridge delay
    await timeout(15*1000)
    // check if rate was updated
    const ethUsd = await instance.ethUsd.call()
    assert.isAbove(ethUsd.toNumber(), 0)

    // web3 below is used to just remove the 10**18 multiplication

    log(`contract state: ETH/USD rate = ${web3.fromWei(ethUsd.toNumber(), 'ether')}`)

    // get rate in kEther (more accurate)
    const phiRate = await instance.getkEthPhiRate.call()
    assert.isAbove(phiRate.toNumber(), 0)
    log(`contract state: ETH/PHI rate = ${web3.fromWei(phiRate, 'ether')}`)
    var priceInUsd = (ethUsd.toNumber() * (1/phiRate.toNumber())) * 1000
    assert.isAbove(priceInUsd, 0)
    log(`contract state: 1 PHI = ${web3.fromWei(priceInUsd, 'ether')} USD`)
    // 0.001% offset from the fixed price is allowed
    var offset = phiRatePreIco * 0.00001
    assert.approximately(priceInUsd, phiRatePreIco, offset)
  })

  it('investment directly to pre-ico should fail during running', function() {
    return instance.sendTransaction({from: accounts[7], value: web3.toWei(1, 'ether')})
    .then(assert.fail)
    .catch((e) => {
      // should throw
      assert.isDefined(e)
      assert.match(e, /revert/)
    })
  });
  var investRecord = []
  const investAndAssert = async (index) => {
    var weiInvested = new bn(web3.toWei(1, 'ether')).toNumber()
    const walletAmountBefore = await web3.eth.getBalance(walletAddr)
    const amountOfToken = await instance.getTokenAmount.call(weiInvested)
    assert.isAbove(amountOfToken.toNumber(), 0)
    const tx = await RcInstance.sendTransaction({from: accounts[7+index], value: weiInvested})
    assert.isNotNull(tx)
    log(`gas cost: ${tx.receipt.cumulativeGasUsed} wei`)
    investRecord.push([weiInvested, amountOfToken.toNumber()])
    var tokenPurchaseEvent = abiDecoder.decodeLogs(tx.receipt.logs)[0]
    // should fire event if token is purchased
    assert.equal(tokenPurchaseEvent.name, 'TokenPurchase')
    assert.equal(tokenPurchaseEvent.address, instance.address) // ico address
    assert.equal(tokenPurchaseEvent.events[0].value, RcInstance.address) // purchaser
    assert.equal(tokenPurchaseEvent.events[1].value, accounts[7+index]) // beneficiary
    assert.equal(tokenPurchaseEvent.events[2].value, weiInvested)
    assert.equal(tokenPurchaseEvent.events[3].value, amountOfToken)
    const balance = await instance.getInvestorBalance.call(accounts[7+index])
    assert.equal(balance.toNumber(), amountOfToken.toNumber())
    log(`Bought ${amountOfToken.toNumber()} tokens (sphi) with ${weiInvested} wei`)
    // check if the wallet balance changed
    const walletAmountNow = await web3.eth.getBalance(walletAddr)
 
    assert.equal(walletAmountNow.minus(walletAmountBefore), weiInvested)

    const balanceWeiInvestor = await instance.balancesWei.call(accounts[7+index])
    assert.equal(balanceWeiInvestor.toNumber(), weiInvested)
  }
  it('investment should succeed if crowdsale is running (startTime)', async () => {
    // check if is running stage
    var nowStage = await instance.currentStage.call()
    assert.equal(nowStage.toNumber(), 1)

    // invest 10 times (from 10 different accounts)
    await investAndAssert(1)
    await investAndAssert(2)
    await investAndAssert(3)
    await investAndAssert(4)
    await investAndAssert(5)
    await investAndAssert(6)
    await investAndAssert(7)
    await investAndAssert(8)
    await investAndAssert(9)
    await investAndAssert(10)

    var weiToInvest = 0
    for (var i = 0; i < investRecord.length; i++) {
      weiToInvest += investRecord[i][0]
    }

    var amountOfToken = 0
    for (var i = 0; i < investRecord.length; i++) {
      amountOfToken += parseInt(investRecord[i][1])
    }

    const weiRaised = await instance.weiRaised.call()
    assert.equal(weiRaised.toNumber(), weiToInvest)

    const tokensSold = await instance.tokensSold.call()
    // approximately and not equal because of amountOfToken sum (not bignumber)
    assert.approximately(tokensSold.toNumber(), amountOfToken, 2000000)
  });

  it('investment should fail if direct to pre-ico', function (){
    return instance.sendTransaction({from: accounts[7], value: web3.toWei(1, 'ether')})
    .then(assert.fail)
    .catch((e) => {
      // should throw
      assert.isDefined(e)
      assert.match(e, /revert/)
    })
  });

  it('investment should fail if it will exceed the total tokens available', function () {
    return instance.sendTransaction({from: accounts[10], value: web3.toWei(4700, 'ether')})   
    .then(assert.fail)
    .catch((e) => {
      // should throw
      assert.isDefined(e)
      assert.match(e, /revert/)
    })
  });

  it('investment should fail after crowdsale has ended (endTime)', async () => {
    // wait 33 seconds (after 30 seconds the crowdsale is closed)
    await timeout(33*1000)
    return RcInstance.sendTransaction({from: accounts[7], value: web3.toWei(1, 'ether')})
    .then(assert.fail)
    .catch((e) => {
      // should throw
      assert.isDefined(e)
      assert.match(e, /revert/)
    })
  });

  it('should change state to Finished', async () => {
      // set finished state
      const tx = await instance.changeStage(4, {from: ownerAddr})
      log(`gas cost: ${tx.receipt.cumulativeGasUsed} wei`)
      // check if stage changed
      var nowStage = await instance.currentStage.call()
      assert.equal(nowStage.toNumber(), 4)
  });

  it('should fail to change state to Running from not the owner', async () => {
    return instance.changeStage(2, {from: accounts[7]})
    .then(assert.fail)
    .catch(async (e) => {
      // should throw
      assert.isDefined(e)
      assert.match(e, /revert/)
      // check if stage NOT changed
      var nowStage = await instance.currentStage.call()
      assert.equal(nowStage.toNumber(), 4)
    })
  });
  var investorsList = [
    accounts[8],
    accounts[9],
    accounts[10],
    accounts[11],
    accounts[12],
    accounts[13],
    accounts[14],
    accounts[15],
    accounts[16],
    accounts[17]
  ]
  var toWhitelist = [
    investorsList[0],
    investorsList[1],
    investorsList[2],
    investorsList[3],
    investorsList[4],
    investorsList[5],
    investorsList[6]
  ]
  // whitelist only the first 7 investors
  it('should whitelist investors', async () => {
    for (var i = 0; i < toWhitelist.length; i++) {
      var tx = await instance.addWhitelistAddress(toWhitelist[i], {from: ownerAddr})
      log(`gas cost: ${tx.receipt.cumulativeGasUsed} wei`)
      var isWhitelistedNow = await instance.isWhitelisted.call(toWhitelist[i])
      assert.equal(isWhitelistedNow, true)
    }

    for (var i = 7; i < 10; i++) {
      // check not whitelisted
      var isWhitelistedNow = await instance.isWhitelisted.call(investorsList[i])
      assert.equal(isWhitelistedNow, false) 
    }
  });

  it('should finalize crowdsale and send tokens', async () => {
    for (var i = 0; i < toWhitelist.length; i++) {
      var tx = await instance.finalizeSingleInvestor(toWhitelist[i], {from: ownerAddr})
      assert.isNotNull(tx)
      log(`gas cost: ${tx.receipt.cumulativeGasUsed} wei`)
      // check if Transfer event was fired
      var transferEv = tx.receipt.logs[0]
      assert.equal(transferEv.topics[0], '0xddf252ad1be2c89b69c2b068fc378daa952ba7f163c4a11628f55a4df523b3ef')
      assert.equal(transferEv.topics[1], instance.address.replace('0x', '0x000000000000000000000000'))
      assert.equal(transferEv.topics[2], toWhitelist[i].replace('0x', '0x000000000000000000000000'))
      assert.equal(web3.toDecimal(transferEv.data), investRecord[i][1])

      // investor balance should be reset (after finalize)
      const balance = await instance.getInvestorBalance.call(toWhitelist[i])
      assert.equal(balance.toNumber(), 0)
      // check if the token was correctly transfered to the investor
      const tokenBalance = await PhiTokenInstance.balanceOf.call(toWhitelist[i])
      assert.equal(tokenBalance.toNumber(), investRecord[i][1])
    }

    var tokenSum = 0;
    for (var i = 0; i < 7; i++) {
      tokenSum += parseInt(investRecord[i][1])
    }

    // check if variable has updated correctly
    const tokensFinalized = await instance.tokensFinalized.call()
    // approx instead of equal because of bignumber and sum
    assert.approximately(tokensFinalized.toNumber(), tokenSum, 300000)
    // check if tokens finalized matches
    const tokensSold = await instance.tokensSold.call()
    var tokensSoldTotal = 0
    for (var i = 0; i < investRecord.length; i++) {
      tokensSoldTotal += parseInt(investRecord[i][1])
    }

    assert.approximately(tokensSold.toNumber(), tokensSoldTotal, 600000)
  });

  it('whitelisted investor should be removed after finalizing', async () => {
    for (var i = 0; i < toWhitelist.length; i++) {
      const isWhitelistedNow = await instance.isWhitelisted.call(toWhitelist[i])
      assert.equal(isWhitelistedNow, false)

      const balanceToken = await instance.balancesToken.call(toWhitelist[i])
      assert.equal(balanceToken.toNumber(), 0)

      // wei amount is not reset (used for reference)
      const balanceWeiInvestor = await instance.balancesWei.call(toWhitelist[i])
      assert.equal(balanceWeiInvestor.toNumber(), investRecord[i][0])
    }
  });

  it('should change stage to Finalized', async () => {
    const tx = await instance.changeStage(5, {from: ownerAddr})
    assert.isNotNull(tx)
    const nowStage = await instance.currentStage.call()
    assert.equal(nowStage.toNumber(), 5)
  });

  it('should burn remaining tokens', async () => {
    const maxTokens = await instance.MAX_TOKENS.call()
    const tokensFinalized = await instance.tokensFinalized.call()
    const toBurn = maxTokens.minus(tokensFinalized)
    
    const phiTokenSupplyBefore = await PhiTokenInstance.totalSupply.call()

    const tx = await instance.burnRemainingTokens({from: ownerAddr})
    log(`gas cost: ${tx.receipt.cumulativeGasUsed} wei`)
    // check Transfer event
    var transferEv = tx.receipt.logs[1]
    assert.equal(transferEv.topics[0], '0xddf252ad1be2c89b69c2b068fc378daa952ba7f163c4a11628f55a4df523b3ef')
    assert.equal(transferEv.topics[1], instance.address.replace('0x', '0x000000000000000000000000'))
    assert.equal(transferEv.topics[2], '0x0000000000000000000000000000000000000000000000000000000000000000')
    assert.equal(web3.toDecimal(transferEv.data), toBurn)

    // check Burn event
    var burntEv = tx.receipt.logs[0]
    assert.equal(burntEv.topics[0], '0xcc16f5dbb4873280815c1ee09dbd06736cffcc184412cf7a71a0fdb75d397ca5')
    assert.equal(burntEv.topics[1], instance.address.replace('0x', '0x000000000000000000000000'))
    
    // we are checking if the total supply (of the phi token) decreased
    const phiTokenSupplyAfter = await PhiTokenInstance.totalSupply.call()
    assert.isBelow(phiTokenSupplyAfter.toNumber(), phiTokenSupplyBefore.toNumber())
    assert.equal(phiTokenSupplyAfter.toNumber(), phiTokenSupplyBefore.minus(toBurn).toNumber())
    assert.equal(phiTokenSupplyBefore.minus(phiTokenSupplyAfter).toNumber(), toBurn.toNumber())
    
    // check burn tokens
    assert.equal(web3.toDecimal(burntEv.data), toBurn)
    assert.equal(web3.toDecimal(burntEv.data), web3.toDecimal(transferEv.data))


    for (var i = 0; i < toWhitelist.length; i++) {
      const tokenBalance = await PhiTokenInstance.balanceOf.call(toWhitelist[i])
      assert.equal(tokenBalance.toNumber(), investRecord[i][1])
    }
    // TODO: check balance of not whitelisted addresses
  });
});
