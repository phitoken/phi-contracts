var PhiToken = artifacts.require("PhiToken")

// testing data
var ico = "0xab24efc62f92a3fc92f4dbe5db13e2fb23dec956"
var preIco = "0x912d92502de8ec2b4057f7f3b39bb67b0418192b"
var walletAddr = null // to be assigned to local account
var lockTime = parseInt(Date.now()/1000) + 30 // 30 seconds lock time (for testing)

var deployBalance = {
  [ico]: 7881196000000000000000000,
  [preIco]: 3524578000000000000000000
}

contract('PhiToken', function(accounts) {
  before(async function() {
    // assign to local address for testing
    walletAddr = accounts[0]
    deployBalance[walletAddr] = 9227465000000000000000000

    console.log()
    console.log(`  Testing variables:\n   lock time: ${lockTime}`)
    console.log(`   ICO address: ${ico}`)
    console.log(`   pre-ICO address: ${preIco}`)
    console.log(`   wallet address: ${walletAddr}`)
    console.log(`   Tokens assigned:`)

    Object.keys(deployBalance).map((address) => {
      console.log(`     ${address}: ${deployBalance[address]} PHI`)
    })
    console.log()

    instance = await PhiToken.new(ico, preIco, walletAddr, lockTime);
  });

  it('verify token metadata', async () => {
    const name = await instance.name.call();
    assert.strictEqual(name, 'PHI Token');

    const decimals = await instance.decimals.call();
    assert.strictEqual(decimals.toNumber(), 18);

    const symbol = await instance.symbol.call();
    assert.strictEqual(symbol, 'PHI');

    const totalSupply = await instance.totalSupply.call();
    assert.strictEqual(totalSupply.toNumber(), 24157817000000000000000000);
  });

  it("verify assigned tokens (on deploy)", function() {
    // check one by one
    instance.balanceOf.call(ico)
    .then((balance) => {
      assert.strictEqual(balance.toNumber(), deployBalance[ico], "ICO balance should be " + deployBalance[ico]);
      return instance.balanceOf.call(preIco);
    }).then((balance) => {
      assert.strictEqual(balance.toNumber(), deployBalance[preIco], "PRE-ICO balance should be " + deployBalance[preIco]);
      return instance.balanceOf.call(walletAddr);
    }).then((balance) => {
      assert.strictEqual(balance.toNumber(), deployBalance[walletAddr], "Wallet address balance should be " + deployBalance[walletAddr]);
    })
  });

  it("verify pre-sale tokens", async () => {

    var addr1 = await instance.balanceOf.call("0x72B16DC0e5f85aA4BBFcE81687CCc9D6871C2965")
    assert.equal(addr1.toNumber(), 230387 * 10**18)

    var addr2 = await instance.balanceOf.call("0x7270cC02d88Ea63FC26384f5d08e14EE87E75154")
    assert.equal(addr2.toNumber(), 132162 * 10**18)

    var addr3 = await instance.balanceOf.call("0x25F92f21222969BB0b1f14f19FBa770D30Ff678f")
    assert.equal(addr3.toNumber(), 132162 * 10**18)

    var addr4 = await instance.balanceOf.call("0xAc99C59D3353a34531Fae217Ba77139BBe4eDBb3")
    assert.equal(addr4.toNumber(), 443334 * 10**18)

    var addr5 = await instance.balanceOf.call("0xbe41D37eB2d2859143B9f1D29c7BC6d7e59174Da")
    assert.equal(addr5.toNumber(), 970826.5 * 10**18)

    var addr6 = await instance.balanceOf.call("0x63e9FA0e43Fcc7C702ed5997AfB8E215C5beE3c9")
    assert.equal(addr6.toNumber(), 970826.5 * 10**18)

    var addr7 = await instance.balanceOf.call("0x95c67812c5c41733419ac3b1916d2f282e7a15a4")
    assert.equal(addr7.toNumber(), 396486 * 10**18)

    var addr8 = await instance.balanceOf.call("0x1f5d30BB328498fF6E09b717EC22A9046C41C257")
    assert.equal(addr8.toNumber(), 20144 * 10**18)

    var addr9 = await instance.balanceOf.call("0x0a1ac564e95dAEDF8d454a3593b75CCdd474fc42")
    assert.equal(addr9.toNumber(), 19815 * 10**18)

    var addr10 = await instance.balanceOf.call("0x0C5448D5bC4C40b4d2b2c1D7E58E0541698d3e6E")
    assert.equal(addr10.toNumber(), 19815 * 10**18)

    var addr11 = await instance.balanceOf.call("0xFAe11D521538F067cE0B13B6f8C929cdEA934D07")
    assert.equal(addr11.toNumber(), 75279 * 10**18)

    var addr12 = await instance.balanceOf.call("0xEE51304603887fFF15c6d12165C6d96ff0f0c85b")
    assert.equal(addr12.toNumber(), 45949 * 10**18)

    var addr13 = await instance.balanceOf.call("0xd7Bab04C944faAFa232d6EBFE4f60FF8C4e9815F")
    assert.equal(addr13.toNumber(), 6127 * 10**18)

    var addr14 = await instance.balanceOf.call("0x603f39C81560019c8360F33bA45Bc1E4CAECb33e")
    assert.equal(addr14.toNumber(), 45949 * 10**18)

    var addr15 = await instance.balanceOf.call("0xBB5128f1093D1aa85F6d7D0cC20b8415E0104eDD")
    assert.equal(addr15.toNumber(), 15316 * 10**18)

  });

  it("transfers should fail during lockTime", function() {
    return instance.transfer(accounts[1], 1000, {from: walletAddr})
   .then(assert.fail)
   .catch((error) => {
      // check for transaction throw
      assert.match(error, /revert/, 'Transfers should throw if lockTime is not passed')

      // since transfer is failed we expect that the account 1 balance is 0
      return instance.balanceOf.call(accounts[1])
   }).then((balance) => {
      assert.strictEqual(balance.toNumber(), 0, "Account balance should be 0");
    })
  })

  it("transfers should work when lockTime passes", function(done) {
    var valueToTransfer = 1000
    // wait lockTime and try to transfer
    setTimeout(() => {
      return instance.transfer(accounts[1], valueToTransfer, {from: walletAddr})
      .then((tx) => {
        // check if transaction was executed correclty
        assert.isObject(tx, 'Tx should not contain errors')
        // check if transfer event was emitted
        assert.equal(tx.logs[0].event, 'Transfer', 'Should emit also Transfer event')
        return instance.balanceOf.call(accounts[1])
      }).then((balance) => {
        assert.strictEqual(balance.toNumber(), valueToTransfer, "Account balance should be updated and equal to " + valueToTransfer);
        done()
      }).catch(e => {
        assert.isNull(e, 'Tx should not contain errors')
      })
    }, 33*1000)
  })



  it('ether transfer should be reversed (fallback fn).', async () => {
    const balanceBefore = await instance.balanceOf.call(accounts[1]);
    assert.strictEqual(balanceBefore.toNumber(), 1000);
    web3.eth.sendTransaction({ from: accounts[1], to: instance.address, value: web3.toWei('0.01', 'Ether') }, async (err, res) => {
      assert.isDefined(err)
      assert.isUndefined(res)
      assert.match(err, /revert/)
      const balanceAfter = await instance.balanceOf.call(accounts[1]);
      assert.strictEqual(balanceAfter.toNumber(), 1000);
    });
  });

  // TRANSERS from eip20 tests
  // normal transfers without approvals
  it('transfers: should transfer 1000 to accounts[2] with accounts[1] having 1000', async () => {
    const balanceStart = await instance.balanceOf.call(accounts[1])
    assert.strictEqual(balanceStart.toNumber(), 1000)
    const tx = await instance.transfer(accounts[2], 1000, { from: accounts[1] });
    assert.isObject(tx)
    // check if balance is 0 now
    const balance = await instance.balanceOf.call(accounts[1]);
    assert.strictEqual(balance.toNumber(), 0);

    const balanceRecv = await instance.balanceOf.call(accounts[2]);
    assert.strictEqual(balanceRecv.toNumber(), 1000);
  });

  // transfers only a portion
  it('transfers: should transfer 50 to accounts[1] with accounts[2] having 1000', async () => {
    const balanceStart = await instance.balanceOf.call(accounts[2])
    assert.strictEqual(balanceStart.toNumber(), 1000)
    // transfer 50 tokens
    const tx = await instance.transfer(accounts[1], 50, { from: accounts[2] });
    assert.isObject(tx)
    // check if balance is 950 now
    const balance = await instance.balanceOf.call(accounts[2]);
    assert.strictEqual(balance.toNumber(), 950);

    // check if tokens are transfered
    const balanceRecv = await instance.balanceOf.call(accounts[1]);
    assert.strictEqual(balanceRecv.toNumber(), 50);
  });

  it('transfers: should fail when trying to transfer 1001 to accounts[1] with accounts[2] having 1000', async () => {
    return instance.transfer(accounts[1], 1001, { from: accounts[2] })
    .then(assert.fail)
    .catch(e => {
      assert.isDefined(e)
      assert.match(e, /revert/)
    })
  });

  it('transfers: should handle zero-transfers normally', async () => {
    assert(await instance.transfer.call(accounts[1], 0, { from: accounts[0] }), 'zero-transfer has failed');
  });

  // APPROVALS
  it('approvals: msg.sender should approve 100 to accounts[4]', async () => {
    // prepare accounts for next tests
    await instance.transfer(accounts[5], 10000, {from: accounts[0]})
    const balance0 = await instance.balanceOf.call(accounts[5]);
    assert.strictEqual(balance0.toNumber(), 10000);

    await instance.approve(accounts[4], 100, {from: accounts[5]});
    const allowance = await instance.allowance.call(accounts[5], accounts[4]);
    assert.strictEqual(allowance.toNumber(), 100);
    // required by the new approve function
    await instance.approve(accounts[4], 0, {from: accounts[5]})
  });

  // bit overkill. But is for testing a bug
  it('approvals: msg.sender approves accounts[4] of 100 & withdraws 20 once.', async () => {
    const balance0 = await instance.balanceOf.call(accounts[5]);
    assert.strictEqual(balance0.toNumber(), 10000);

    await instance.approve(accounts[4], 100, { from: accounts[5] }); // 100
    const balance2 = await instance.balanceOf.call(accounts[3]);
    assert.strictEqual(balance2.toNumber(), 0, 'balance2 not correct');
    
    await instance.transferFrom.call(accounts[5], accounts[3], 20, { from: accounts[4] });
    var allw1 = await instance.allowance.call(accounts[5], accounts[4]);

    await instance.transferFrom(accounts[5], accounts[3], 20, { from: accounts[4] }); // -20
    const allowance01 = await instance.allowance.call(accounts[5], accounts[4]);
    assert.strictEqual(allowance01.toNumber(), 80); // =80
    
    const balance22 = await instance.balanceOf.call(accounts[3]);
    assert.strictEqual(balance22.toNumber(), 20);

    const balance02 = await instance.balanceOf.call(accounts[5]);
    assert.strictEqual(balance02.toNumber(), 9980);
  });

  it('approvals: attempt withdrawal from account with no allowance (should fail)', () => {
    return instance.transferFrom(accounts[5], accounts[3], 60, { from: accounts[0] })
    .then(assert.fail)
    .catch(e => {
      assert.isDefined(e)
      assert.match(e, /revert/)
    })
  })

  it('approvals: approve max (2^256 - 1)', async () => {
    // reset
    await instance.approve(accounts[4], '0', { from: accounts[5] });

    await instance.approve(accounts[4], '115792089237316195423570985008687907853269984665640564039457584007913129639935', { from: accounts[5] });
    const allowance = await instance.allowance(accounts[5], accounts[4]);
    assert(allowance.equals('1.15792089237316195423570985008687907853269984665640564039457584007913129639935e+77'));
  });

  it('events: should fire Transfer event properly', async () => {
    const res = await instance.transfer(accounts[4], '2666', { from: accounts[5] });
    const transferLog = res.logs.find(element => element.event.match('Transfer'));
    assert.strictEqual(transferLog.args._from, accounts[5]);
    assert.strictEqual(transferLog.args._to, accounts[4]);
    assert.strictEqual(transferLog.args._value.toString(), '2666');
  });

  it('events: should fire Transfer event normally on a zero transfer', async () => {
    const res = await instance.transfer(accounts[4], '0', { from: accounts[5] });
    const transferLog = res.logs.find(element => element.event.match('Transfer'));
    assert.strictEqual(transferLog.args._from, accounts[5]);
    assert.strictEqual(transferLog.args._to, accounts[4]);
    assert.strictEqual(transferLog.args._value.toString(), '0');
  });

  it('events: should fire Approval event properly', async () => {
    // reset approve
    await instance.approve(accounts[4], '0', { from: accounts[5] })
    const res = await instance.approve(accounts[4], '2666', { from: accounts[5] });
    const approvalLog = res.logs.find(element => element.event.match('Approval'));
    assert.strictEqual(approvalLog.args._owner, accounts[5]);
    assert.strictEqual(approvalLog.args._spender, accounts[4]);
    assert.strictEqual(approvalLog.args._value.toString(), '2666');
  });

  it('events: should fire Transfer event via transferFrom', async () => {
    const res = await instance.transferFrom(accounts[5], accounts[3], '10', { from: accounts[4] });
    const transferLog = res.logs.find(element => element.event.match('Transfer'));
    assert.strictEqual(transferLog.args._from, accounts[5]);
    assert.strictEqual(transferLog.args._to, accounts[3]);
    assert.strictEqual(transferLog.args._value.toString(), '10');
  });

  it('should burn tokens', async () => {
    const balance = await instance.balanceOf.call(accounts[1]);
    const supplyBefore = await instance.totalSupply.call();
    await instance.burn(balance, {from: accounts[1]})
    const supplyAfter = await instance.totalSupply.call();
    const balanceAfter = await instance.balanceOf.call(accounts[1]);
    assert.equal(balanceAfter.toNumber(), 0)
    assert.isBelow(supplyAfter, supplyBefore)
    assert.equal(supplyAfter.toNumber(), supplyBefore - balance)
  });

  it('should fail to burn tokens (token holder with 0 balance)', async () => {
    const balance = await instance.balanceOf.call(accounts[1]);
    assert.equal(balance.toNumber(), 0)
    return instance.burn(1000000, {from: accounts[1]})
    .then(assert.fail)
    .catch((e) => {
      // should throw
      assert.isDefined(e)
      assert.match(e, /revert/)
    })
  });
});
