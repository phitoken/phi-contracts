var Crowdsale = artifacts.require("Ico")
// get also PhiToken
var PhiToken = artifacts.require("PhiToken")

// use bn library to convert and do math operations on big numbers
const bn = require('bignumber.js')

const abiDecoder = require('abi-decoder')
abiDecoder.addABI(Crowdsale.abi)

// testing data
var walletAddr = null
var ownerAddr = null
const maxTokenToBeSold = 7881196000000000000000000 // tokens to be sold on ico
const phiRatePreIco = 1618033990000000000 // ico fixed price 1,61803399 * 10**18
const startTime = parseInt(Date.now()/1000) + 30 // start in 30 secs
const endTime = startTime + 120 // end after 120 secs
const intervalUpdate = 30 // oraclize updates in seconds
var instance = null
var PhiTokenInstance = null

/**
 * Returns a random integer between min (inclusive) and max (inclusive)
 * Using Math.round() will give you a non-uniform distribution!
 */
function getRandomInt(min, max) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
}

// timeout helper function
function timeout(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
}
// log helper
function log(txt) {
  console.log(`    ⬐ ${txt}`)
}

contract('Oraclize queries test', function(accounts) {
  before(async function() {
    walletAddr = accounts[1]
    ownerAddr = accounts[2]
    // deploy new instances for the tests
    instance = await Crowdsale.new(walletAddr, {from: ownerAddr})
    PhiTokenInstance = await PhiToken.new(instance.address, accounts[4], walletAddr, parseInt(Date.now()/1000) + 600, {from: ownerAddr})
  });

  it('verify crowdsale deploy state', async () => {
    const owner = await instance.owner.call();
    assert.equal(owner, ownerAddr);

    const wallet = await instance.wallet.call();
    assert.equal(wallet, walletAddr);

    // check stage
    const nowStage = await instance.currentStage.call();
    // should be 0 = ToInitialize, // Crowdsale has not been initialized
    assert.strictEqual(nowStage.toNumber(), 0);

    const MAX_TOKENS = await instance.MAX_TOKENS.call();
    assert.strictEqual(MAX_TOKENS.toNumber(), maxTokenToBeSold);

    const phiRate = await instance.phiRate.call();
    assert.strictEqual(phiRate.toNumber(), phiRatePreIco);
  });

  it('should init crowdsale (succeed)', function() {
    return instance.initializeCrowdsale(startTime, endTime, PhiTokenInstance.address, intervalUpdate, {from: ownerAddr, value: 1e18})
    .then((tx) => {
      assert.isNotNull(tx)
      log(`gas cost: ${tx.receipt.cumulativeGasUsed} wei`)
      // check if init event is fired
      assert.equal(tx.logs[0].event, 'LogCrowdsaleInit')
      // check if Oraclize event is fired (oraclize_query)
      assert.equal(tx.receipt.logs[0].topics[0], '0xb76d0edd90c6a07aa3ff7a222d7f5933e29c6acc660c059c97837f05c4ca1a84')
    })
  });

  it('check ETH/USD rate and PHI rate before the official startTime', async () => {
    // wait 30 seconds
    await timeout(30*1000)
    // wait 15 seconds because of the ethereum-bridge delay
    await timeout(15*1000)
    // check if rate was updated
    const ethUsd = await instance.ethUsd.call()
    assert.isAbove(ethUsd.toNumber(), 0)

    // web3 below is used to just remove the 10**18 multiplication

    log(`contract state: ETH/USD rate = ${web3.fromWei(ethUsd.toNumber(), 'ether')}`)

    // get rate in kEther (more accurate)
    const phiRate = await instance.getkEthPhiRate.call()
    assert.isAbove(phiRate.toNumber(), 0)
    log(`contract state: ETH/PHI rate = ${web3.fromWei(phiRate, 'ether')}`)
    var priceInUsd = (ethUsd.toNumber() * (1/phiRate.toNumber())) * 1000
    assert.isAbove(priceInUsd, 0)
    log(`contract state: 1 PHI = ${web3.fromWei(priceInUsd, 'ether')} USD`)
    // 0.001% offset from the fixed price is allowed
    var offset = phiRatePreIco * 0.00001
    assert.approximately(priceInUsd, phiRatePreIco, offset)

    // get rate with 1 ETH
    var rate1Eth = await instance.getTokenAmount.call(web3.toWei(1, 'ether'))
    log(`1 ETH = ${rate1Eth/10**18} PHI`)
    assert.approximately(rate1Eth/10**18, phiRate/1000, 1)
  })

  it('should catch rate price update (by oracle)', async () => {
  	var num = 0
  	instance.LogRateUpdate().watch((err, res) => {
	    if (err) console.log(err);
	    num += 1
        assert.equal(res.event, 'LogRateUpdate');
    });
    await timeout(40 * 1000 * 4)
    assert.isAtLeast(num, 4)
    const stage = await instance.currentStage.call()
    // finished
    assert.equal(stage.toNumber(), 4)
  })
});
