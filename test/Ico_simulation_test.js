var Crowdsale = artifacts.require("Ico")
// get also PhiToken
var PhiToken = artifacts.require("PhiToken")

const ew = require('ethereumjs-wallet')
const etx = require('ethereumjs-tx')

// use bn library to convert and do math operations on big numbers
const bn = require('bignumber.js')

const abiDecoder = require('abi-decoder')
abiDecoder.addABI(Crowdsale.abi)

// testing data
var walletAddr = null
var ownerAddr = null
const maxTokenToBeSold = 7881196000000000000000000 // tokens to be sold on ico
const phiRateIco = 1618033990000000000 // ico fixed price 1,61803399 * 10**18
const startTime = parseInt(Date.now()/1000) + 30 // start in 30 secs
const endTime = startTime + 90 // end after 90 secs
const intervalUpdate = 60 // oraclize updates in seconds
var instance = null
var PhiTokenInstance = null

var investorBalanceTest1 = null

var PhiTokenInstance = null

/**
 * Returns a random integer between min (inclusive) and max (inclusive)
 * Using Math.round() will give you a non-uniform distribution!
 */
function getRandomInt(min, max) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
}

// timeout helper function
function timeout(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
}
// log helper
function log(txt) {
  console.log(`    ⬐ ${txt}`)
}

var testBuyers = []

contract('Ico simulation', function(accounts) {
  before(async function() {
    walletAddr = accounts[1]
    ownerAddr = accounts[2]
    // deploy new instances for the tests
    instance = await Crowdsale.new(walletAddr, {from: ownerAddr})
    PhiTokenInstance = await PhiToken.new(instance.address, accounts[4], walletAddr, parseInt(Date.now()/1000) + 600, {from: ownerAddr})
  });

  it('verify crowdsale deploy state', async () => {
    const owner = await instance.owner.call();
    assert.equal(owner, ownerAddr);

    const wallet = await instance.wallet.call();
    assert.equal(wallet, walletAddr);

    // check stage
    const nowStage = await instance.currentStage.call();
    // should be 0 = ToInitialize, // Crowdsale has not been initialized
    assert.strictEqual(nowStage.toNumber(), 0);

    const MAX_TOKENS = await instance.MAX_TOKENS.call();
    assert.strictEqual(MAX_TOKENS.toNumber(), maxTokenToBeSold);

    const phiRate = await instance.phiRate.call();
    assert.strictEqual(phiRate.toNumber(), phiRateIco);
  });

  it('investment should fail before initialize', function() {
    return instance.sendTransaction({from: accounts[7], value: web3.toWei(1, 'ether')})
    .then(assert.fail)
    .catch((e) => {
      // should throw
      assert.isDefined(e)
      assert.match(e, /revert/)
    })
  });

  it('should init crowdsale (succeed)', function() {
    return instance.initializeCrowdsale(startTime, endTime, PhiTokenInstance.address, intervalUpdate, {from: ownerAddr, value: 1e8})
    .then((tx) => {
      assert.isNotNull(tx)
      // check if init event is fired
      assert.equal(tx.logs[0].event, 'LogCrowdsaleInit')
      // check if Oraclize event is fired (oraclize_query)
      assert.equal(tx.receipt.logs[0].topics[0], '0xb76d0edd90c6a07aa3ff7a222d7f5933e29c6acc660c059c97837f05c4ca1a84')
    })
  });

  it('investment should fail if crowdsale is not running (startTime)', function() {
    return instance.sendTransaction({from: accounts[7], value: 1e8})
    .then(assert.fail)
    .catch((e) => {
      // should throw
      assert.isDefined(e)
      assert.match(e, /revert/)
    })
  });

  it('investment to ico should fail before startTime', function() {
    return instance.sendTransaction({from: accounts[7], value: web3.toWei(1, 'ether')})
    .then(assert.fail)
    .catch((e) => {
      // should throw
      assert.isDefined(e)
      assert.match(e, /revert/)
    })
  });

  it('check ETH/USD rate and PHI rate before the official startTime', async () => {
    // wait 30 seconds
    await timeout(30*1000)
    // wait 15 seconds because of the ethereum-bridge delay
    await timeout(15*1000)
    // check if rate was updated
    const ethUsd = await instance.ethUsd.call()
    assert.isAbove(ethUsd.toNumber(), 500)

    // web3 below is used to just remove the 10**18 multiplication

    log(`contract state: ETH/USD rate = ${web3.fromWei(ethUsd.toNumber(), 'ether')}`)

    // get rate in kEther (more accurate)
    const phiRate = await instance.getkEthPhiRate.call()
    assert.isAbove(phiRate.toNumber(), 0)
    log(`contract state: ETH/PHI rate = ${web3.fromWei(phiRate, 'ether')}`)
    var priceInUsd = (ethUsd.toNumber() * (1/phiRate.toNumber())) * 1000
    assert.isAbove(priceInUsd, 0)
    log(`contract state: 1 PHI = ${web3.fromWei(priceInUsd, 'ether')} USD`)
    // 0.001% offset from the fixed price is allowed
    var offset = phiRateIco * 0.00001
    assert.approximately(priceInUsd, phiRateIco, offset)
  })

  var gasCostSum = 0
  var txContainer = []
  var weiToInvestEachBuyer = 0
  var amountOfTokenPerBuyer = 0
  it("should invest 100 buyers", async () => {
    const ethPhiRate = await instance.getEthPhiRate.call()
    weiToInvestEachBuyer = parseInt(web3.toWei(0.1, 'ether'))
    amountOfTokenPerBuyer = await instance.getTokenAmount.call(weiToInvestEachBuyer)
    // check if calculated token matches (allow some offset)
    //assert.approximately(tokenAmountWei, amountOfTokenPerBuyer)
    // generate 100 private keys and fund them
    log('Generating and funding addresses...')
    for (var i = 0; i < 100; i++) {
      var newAcc = new ew.generate()
      var txFund = {
        from: accounts[getRandomInt(0,49)],
        to: '0x' + newAcc.getAddress().toString('hex'),
        value: weiToInvestEachBuyer + 1e18 // add more because of gas cost/limit
      }
      await web3.eth.sendTransaction(txFund)
      testBuyers.push(newAcc)
    }
    log(`Investing ${weiToInvestEachBuyer} wei for each buyer`)
    const walletBalanceBefore = web3.eth.getBalance(walletAddr)
    for (var i = 0; i < testBuyers.length; i++) {
      var txParams = {
        to: instance.address, 
        value: web3.toHex(weiToInvestEachBuyer),
        gasPrice: web3.toHex(web3.eth.gasPrice), 
        gasLimit: web3.toHex(800000)
      }
      var tx = new etx(txParams)
      tx.sign(testBuyers[i].getPrivateKey())
      var serializedTx = '0x' + tx.serialize().toString('hex')
      // send
      var txBuy = await web3.eth.sendRawTransaction(serializedTx)
      txBuy = await web3.eth.getTransactionReceipt(txBuy)
      txBuy.from = '0x' + testBuyers[i].getAddress().toString('hex')
      txContainer.push(txBuy)
      gasCostSum += txBuy.cumulativeGasUsed
    }
    // store after balance
    const walletBalanceAfter = web3.eth.getBalance(walletAddr)

    log(`Average gas cost for investment: ${parseInt(gasCostSum/100)} - first: ${parseInt(txContainer[0].cumulativeGasUsed)} - last: ${parseInt(txContainer[99].cumulativeGasUsed)}`)
    var percIncrease = parseInt((txContainer[1].cumulativeGasUsed/txContainer[99].cumulativeGasUsed)*100)
    log(`Increase of gast cost from the second to the last: ${percIncrease} %`)
    // check if events were emitted for all the buyers
    for (var i = 0; i < txContainer.length; i++) {
      var decoded = abiDecoder.decodeLogs(txContainer[i].logs)[1]
      // check event fields
      assert.equal(decoded.name, 'TokenPurchase')
      assert.equal(decoded.address, instance.address)
      assert.equal(decoded.events[0].value, txContainer[i].from) // purchaser
      assert.equal(decoded.events[1].value, txContainer[i].from) // beneficiary
      assert.equal(decoded.events[2].value, weiToInvestEachBuyer)
      assert.equal(decoded.events[3].value, amountOfTokenPerBuyer.toNumber())
      // check if balance updated
      var balance = await instance.getInvestorBalance.call(txContainer[i].from)
      assert.equal(balance.toNumber(), amountOfTokenPerBuyer.toNumber())

      var balanceWeiInvestor = await instance.balancesWei.call(txContainer[i].from)
      assert.equal(balanceWeiInvestor.toNumber(), weiToInvestEachBuyer)
    }

    assert.isAbove(walletBalanceAfter.toNumber(), walletBalanceBefore.toNumber())
    assert.equal(walletBalanceAfter.minus(walletBalanceBefore).minus(weiToInvestEachBuyer * 100).toNumber(), 0)

    const tokensSold = await instance.tokensSold.call()
    log(`Sold ${tokensSold.toNumber()} of ${maxTokenToBeSold}`)
    assert.equal(tokensSold.toNumber(), amountOfTokenPerBuyer * 100)
  })

  // TODO check RC balances
  // TODO check balanceswei

  it('should change state to Finished', async () => {
      // wait for end
      await timeout(31 * 1000)
      // set finished state
      const tx = await instance.changeStage(4, {from: ownerAddr})
      log(`gas cost: ${tx.receipt.cumulativeGasUsed} wei`)
      // check if stage changed
      var nowStage = await instance.currentStage.call()
      assert.equal(nowStage.toNumber(), 4)
  });

  var investors = []
  it('should whitelist 100 investors', async () => {
    investors = txContainer.map((tx) => {
      return tx.from
    })
    assert.equal(investors.length, 100)
    // whitelist the investor on the test before
    const tx = await instance.addWhitelistAddrByList(investors, {from: ownerAddr})
    log(`gas cost: ${tx.receipt.cumulativeGasUsed} wei`)
    // check if the whitelisted address was added at the correct index
    for (var i = 0; i < 100; i++) {
      var addressAtIndex = await instance.getWhitelistedAddrAt.call(i)
      assert.equal(addressAtIndex.toString(), investors[i]) 
      const isWhitelistedNow = await instance.isWhitelisted.call(investors[i])
      assert.equal(isWhitelistedNow, true)
    }
  });

  it('should finalize 100 investors', async () => {
    assert.equal(investors.length, 100)
    // finalize
    const tx = await instance.finalize({from: ownerAddr})
    log(`gas cost: ${tx.receipt.cumulativeGasUsed} wei`)
    // check if the balance on the crowdsale changed
    for (var i = 0; i < 100; i++) {
      var addressAtIndex = await instance.getInvestorBalance.call(investors[i])
      assert.equal(addressAtIndex.toNumber(), 0) 
    }
    // check if the balance was updated on the phi token
    for (var i = 0; i < 100; i++) {
      var addressAtIndex = await PhiTokenInstance.balanceOf.call(investors[i])
      assert.equal(addressAtIndex.toNumber(), amountOfTokenPerBuyer.toNumber()) 
    }
    // check Transfer event
    for (var i = 0; i < 100; i++) {
      assert.equal(tx.receipt.logs[i].topics[0], '0xddf252ad1be2c89b69c2b068fc378daa952ba7f163c4a11628f55a4df523b3ef')
      // check from and to and token amount of the event
      assert.equal(tx.receipt.logs[i].topics[1], instance.address.replace('0x', '0x000000000000000000000000'))
      assert.equal(tx.receipt.logs[i].topics[2], investors[i].replace('0x', '0x000000000000000000000000'))
      assert.equal(web3.toDecimal(tx.receipt.logs[i].data), amountOfTokenPerBuyer)
    }
  });

  it('whitelisted investor should be removed after finalizing', async () => {
    for (var i = 0; i < investors.length; i++) {
      const isWhitelistedNow = await instance.isWhitelisted.call(investors[i])
      const addressAtIndex = await instance.getWhitelistedAddrAt.call(i)
      assert.equal(addressAtIndex.toString(), '0x0000000000000000000000000000000000000000')
      assert.equal(isWhitelistedNow, false)

      // balances in wei are not reset
      var balanceWeiInvestor = await instance.balancesWei.call(investors[i])
      assert.equal(balanceWeiInvestor.toNumber(), weiToInvestEachBuyer)
    }
  });

  it('should change stage to Finalized', async () => {
    const tx = await instance.changeStage(5, {from: ownerAddr})
    assert.isNotNull(tx)
    const nowStage = await instance.currentStage.call()
    assert.equal(nowStage.toNumber(), 5)
  });

  it('should burn remaining tokens', async () => {
    const phiSupplyBefore = await PhiTokenInstance.totalSupply.call()
    const tx = await instance.burnRemainingTokens({from: ownerAddr})
    const phiSupplyAfter = await PhiTokenInstance.totalSupply.call()
    const tokensFinalized = await instance.tokensFinalized.call()
    assert.isBelow(phiSupplyAfter, phiSupplyBefore)
    var toBurn = maxTokenToBeSold - tokensFinalized
    // check amount of burn tokens
    assert.equal(phiSupplyBefore.minus(phiSupplyAfter).toNumber(), toBurn)

    // check Transfer event
    var transferEv = tx.receipt.logs[1]
    assert.equal(transferEv.topics[0], '0xddf252ad1be2c89b69c2b068fc378daa952ba7f163c4a11628f55a4df523b3ef')
    assert.equal(transferEv.topics[1], instance.address.replace('0x', '0x000000000000000000000000'))
    assert.equal(transferEv.topics[2], '0x0000000000000000000000000000000000000000000000000000000000000000')
    assert.equal(web3.toDecimal(transferEv.data), toBurn)

    // check Burn event
    var burntEv = tx.receipt.logs[0]
    assert.equal(burntEv.topics[0], '0xcc16f5dbb4873280815c1ee09dbd06736cffcc184412cf7a71a0fdb75d397ca5')
    assert.equal(burntEv.topics[1], instance.address.replace('0x', '0x000000000000000000000000'))
    
    // check burn tokens
    assert.equal(web3.toDecimal(burntEv.data), toBurn)
    assert.equal(web3.toDecimal(burntEv.data), web3.toDecimal(transferEv.data))
  })
});
