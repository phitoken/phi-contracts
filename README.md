### PHI contracts

Contains token, pre-ico, ico, reservation contract smart contracts and tests.

Check out the other readme for installation/reqs.:

 * [PHI token](README_PHI_TOKEN.md)
 * [pre ICO](README_PRE_ICO.md)
 * [ICO](README_ICO.md)
 * [Reservation contract](README_RC.md)
