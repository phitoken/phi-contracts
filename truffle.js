module.exports = {
  networks: {
  	"development": {
  	  host: "127.0.0.1",
  	  port: 8545,
  	  network_id: "*", // Match any network id
  	  gasPrice: 8000000000
  	}
  },
  solc: {
    optimizer: {
      "enabled": true,
      "runs": 200
    }
  }
};
