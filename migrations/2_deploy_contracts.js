var PhiToken = artifacts.require("./PhiToken.sol");
var PreIco = artifacts.require("./PreIco.sol");
var Ico = artifacts.require("./Ico.sol");
var RC = artifacts.require("./ReservationContract.sol");
var config = require('../main_config.json')
var contract = require('truffle-contract')

module.exports = async function(deployer, network, accounts) {
  // migrations, needed for truffle to work, to be changed with a config file
  await deployer.deploy(PreIco, accounts[2], {from: accounts[1]})
  await deployer.deploy(Ico, accounts[2], {from: accounts[1]})
  await deployer.deploy(PhiToken,
    Ico.address, // ICO
    PreIco.address, // pre-ICO address
    accounts[2], // wallet
    config.token.lock_time // lockTime: Thursday, April 5th 2018, 13:00:00 GMT+1
  )
  var PreIcoInstance = await PreIco.deployed()
  await PreIcoInstance.initializeCrowdsale(
    config.pre_ico.start,
    config.pre_ico.end,
    PhiToken.address,
    config.interval_update,
    {from: accounts[1], value: 1e8}
  )
  return deployer.deploy(RC, PreIcoInstance.address, {from: accounts[1]})
};
