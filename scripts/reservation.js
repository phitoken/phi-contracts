#!/usr/bin/env node
require('dotenv').config()
const Rc = require('../build/contracts/ReservationContract.json')

const BigNumber = require('bignumber.js')
const program = require('commander')
const assert = require('assert')
const contract = require("truffle-contract")
const HDWalletProvider = require("truffle-hdwallet-provider")
const Web3 = require('web3')
const utils = require('./utils.js')
const saveAddress = utils.saveAddress

var Confirm = require('prompt-confirm')
var prompt = new Confirm('Are you sure you want to continue?')
const yn = require('yn')

// config
const config = require('../main_config.json')

const w3u = require('web3-utils')

program
  .version('0.1.0')
  .option('--gas-price [wei]', 'Gas price (default: 1 gwei)', 1000000000)
  .option('-a, --address [address]', 'Pre-ICO Contract address')
  .option('--deploy', 'Deploy contract')
  .option('-c, --cheese [type]', 'Add the specified type of cheese [marble]', 'marble')
  .parse(process.argv);

var provider = null
if (process.env.DEVELOPMENT == 'true') {
  console.log('Using dev node')
  provider = new HDWalletProvider(process.env.MNEMONIC, process.env.DEV_NODE, 0)
} else {
  console.log('Using mainnet node')
  provider = new HDWalletProvider(process.env.MNEMONIC, process.env.MAINNET_NODE, 0)
}

const DEPLOY_ADDR = provider.address
console.log('Using address: ' + DEPLOY_ADDR)

var RcContract = contract(Rc)
// set provider
RcContract.setProvider(provider)

const deployRc = async () => {
  const preIco = program.address
  console.log('Deploying Reservation Contract with pre-ico set to: ' + preIco)
  var reply = await prompt.run()
  if (!yn(reply)) process.exit(1)
  var tx = await RcContract.new(
    preIco,
    {from: DEPLOY_ADDR, gasPrice: program.gasPrice}
  )
  // TODO assert constants
  console.log('Reservation contract deployed @ ' + tx.address + ' - tx hash: ' + tx.transactionHash)
}

if (program.deploy) {
  if (!program.address) {
    console.log('Requires pre-ico address (--address [pre-ico])')
    process.exit(1)
  }
  deployRc()
}
