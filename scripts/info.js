var Web3 = require('web3');
var web3 = new Web3(new Web3.providers.HttpProvider("https://mainnet.infura.io/AFlhXbMO0Pc9gaSgZwJc"));
var abi = [
    {
        "constant": true, 
        "inputs": [],
        "name": "price",
        "outputs": [
            { "name": "", "type": "uint256" }
        ],
        "payable": false,
        "stateMutability": "view",
        "type": "function"
    },
    {
        "constant": true, 
        "inputs": [],
        "name": "tokensSold",
        "outputs": [
            { "name": "", "type": "uint256" }
        ],
        "payable": false,
        "stateMutability": "view",
        "type": "function"
    },
    {
        "constant": true, 
        "inputs": [],
        "name": "weiRaised",
        "outputs": [
            { "name": "", "type": "uint256" }
        ],
        "payable": false,
        "stateMutability": "view",
        "type": "function"
    },
    {
        "constant": true, 
        "inputs": [],
        "name": "totalTokens",
        "outputs": [
            { "name": "", "type": "uint256" }
        ],
        "payable": false,
        "stateMutability": "view",
        "type": "function"
    },
    {
        "constant": true, 
        "inputs": [],
        "name": "ethUsd",
        "outputs": [
            { "name": "", "type": "uint256" }
        ],
        "payable": false,
        "stateMutability": "view",
        "type": "function"
    },
    {
        "constant": true, 
        "inputs": [],
        "name": "remainingTokens",
        "outputs": [
            { "name": "", "type": "uint256" }
        ],
        "payable": false,
        "stateMutability": "view",
        "type": "function"
    },
    {
        "constant": true, 
        "inputs": [],
        "name": "MAX_TOKENS",
        "outputs": [
            { "name": "", "type": "uint256" }
        ],
        "payable": false,
        "stateMutability": "view",
        "type": "function"
    }
]

async function main () {
    var address = process.argv[2];
    if (typeof address == 'undefined' || address.length <= 30) console.log('enter address')
    var crowdsale = new web3.eth.Contract(abi, address);
    var ethUsd = await crowdsale.methods.ethUsd().call();
    console.log('[ETH/USD] ', ethUsd / 10**18 + ' USD');

    var weiRaised = await crowdsale.methods.weiRaised().call();
    console.log('[ETH RAISED] ', web3.utils.fromWei(weiRaised, 'ether') + ' ETH');
    console.log('in USD: ', web3.utils.fromWei(weiRaised, 'ether') * ethUsd/10**18 + ' $');
    console.log()
    var tokensSold = await crowdsale.methods.tokensSold().call();
    console.log('[TOKENS SOLD] ', tokensSold / 10**18 + ' PHI');

    var remainingTokens = await crowdsale.methods.remainingTokens().call();
    console.log('[REMAINING TOKENS] ', remainingTokens / 10**18 + ' PHI');

    var maxTokens = await crowdsale.methods.MAX_TOKENS().call();

    console.log('[SALE PROGRESS] ' + parseInt((tokensSold/maxTokens) * 100) + ' %')

    if (maxTokens/10**18 != (parseInt(remainingTokens) + parseInt(tokensSold))/10**18) {
        console.log('*** tokens amount not matching')
    }
}

main()
