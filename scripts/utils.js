const fs = require('fs')

module.exports.saveAddress = (param, value) => {
  var location = '../main_config.json'
  var current = JSON.parse(fs.readFileSync(location).toString())
  current[param].address = value
  fs.writeFileSync(location, JSON.stringify(current, null, 4))
  config = require(location)
}
