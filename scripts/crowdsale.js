#!/usr/bin/env node
require('dotenv').config()

const PreIco = require('../build/contracts/PreIco.json')
const Ico = require('../build/contracts/Ico.json')
const Rc = require('../build/contracts/ReservationContract.json')

const BigNumber = require('bignumber.js')
const _ = require('lodash')
const abi = require('ethereumjs-abi')
const json2csv = require('json2csv')
const fs = require('fs')
const program = require('commander')
const assert = require('assert')
const contract = require("truffle-contract")
const HDWalletProvider = require("truffle-hdwallet-provider")
const utils = require('./utils.js')
const saveAddress = utils.saveAddress
const Web3 = require('web3')
const request = require('request-promise')

var Confirm = require('prompt-confirm')
var prompt = new Confirm('Are you sure you want to continue?')
const yn = require('yn')

// config
var config = require('../main_config.json')

const w3u = require('web3-utils')

program
  .version('0.1.0')
  .option('--gas-price [wei]', 'Gas price (default: 1 gwei)', 1000000000)
  .option('--value [wei]', 'Amount to send along this function call (wei)')
  .option('--pre-ico', 'Pre ico contract mode')
  .option('--ico', 'ICO contract mode')
  .option('-a, --address [address]', 'Contract address')
  .option('--burn-unsold', 'Burn unsold tokens')
  .option('--info', 'Get contract info')
  .option('--deploy', 'Deploy contract')
  .option('--initialize', 'Initialize contract')
  .option('--whitelist [file-path]', 'Load the provided JSON array and whitelist the addresses')
  .option('--finalize [file-path]', 'Load the provided JSON array and finalize the addresses')
  .option('--top-up', 'Top up balance')
  .option('--withdraw', 'Withdraw balance in the contract')
  .option('--export', 'Export buyers list with invested WEI and PHI to csv and save also buyers list')
  .option('--add-reservation [address]', 'Add a new reservation contract')
  .option('--remove-reservation [address]', 'Remove a reservation contract')
  .option('--set-stage [stage]', 'Set a stage')
  .option('--verify', 'Verify whitelist addresses and investors balance')
  .option('--finalized', 'Check if addresses are finalized')
  .option('--balances [file-path]', 'File path of the investors balance (JSON)')
  .option('--update-rate', 'Update ETH/USD rate manually (get it by coincompare)')
  .option('--request-rate-update', 'Request ETH/USD rate update from oraclize (on the smart contract)')
  .option('--oraclize-gas-limit [new-gas-wei]', 'Set a new oraclize gas limit')
  .option('--oraclize-gas-price [new-gas-wei]', 'Set a new oraclize gas price')
  .parse(process.argv);

// e.g. --pre-ico --deploy

var provider = null
if (process.env.DEVELOPMENT == 'true') {
  console.log('Using dev node')
  provider = new HDWalletProvider(process.env.MNEMONIC, process.env.DEV_NODE, 0)
} else {
  console.log('Using mainnet node')
  provider = new HDWalletProvider(process.env.MNEMONIC, process.env.MAINNET_NODE, 0)
}

const DEPLOY_ADDR = provider.address
console.log('Using address: ' + DEPLOY_ADDR)

var PreIcoContract = contract(PreIco)
var IcoContract = contract(Ico)
var RcContract = contract(Rc)

// set provider
PreIcoContract.setProvider(provider)
IcoContract.setProvider(provider)
RcContract.setProvider(provider)

const deployContract = async (type) => {
  var walletToSet = type == 'pre_ico' ? config.pre_ico.wallet : config.ico.wallet
  console.log('Deploying ' + type + ' contract with wallet set to: ' + walletToSet)
  var reply = await prompt.run()
  if (!yn(reply)) process.exit(1)
  if (type == 'pre_ico') {
    var tx = await PreIcoContract.new(
      walletToSet,
      {from: DEPLOY_ADDR, gasPrice: program.gasPrice}
    )
  } else if (type == 'ico') {
    var tx = await IcoContract.new(
      walletToSet,
      {from: DEPLOY_ADDR, gasPrice: program.gasPrice}
    )
  }
  saveAddress(type, tx.address)
  console.log(type + 'deployed @ ' + tx.address + ' - tx hash: ' + tx.transactionHash)
}

const updateContractAddress = async (type) => {
  var address = config[type].address
  if (program.address) var address = program.address
  var maxTokens = 0
  if (type == 'pre_ico') {
    PreIcoContract = await PreIcoContract.at(address)
    maxTokens = await PreIcoContract.MAX_TOKENS.call()
  } else if (type == 'ico') {
    IcoContract = await IcoContract.at(address)
    maxTokens = await IcoContract.MAX_TOKENS.call()
  }
  assert.equal(maxTokens.toNumber(), config[type].to_sold * 10**18)
  console.log(type + ' contract set to: ' + address)
}

const changeOraclizeGasLimit = async (type) => {
  var currentGasLimit = 0
  if (type == 'pre_ico') {
    currentGasLimit = await PreIcoContract.ORACLIZE_GAS_LIMIT.call()
  } else if (type == 'ico') {
    currentGasLimit = await IcoContract.ORACLIZE_GAS_LIMIT.call()
  }
  const newGas = parseInt(program.oraclizeGas)
  console.log('Changing Oraclize gas limit, current one: ' + currentGasLimit + ' wei, new one: ' + newGas + ' wei')

  var reply = await prompt.run()
  if (!yn(reply)) process.exit(1)
  var gasSet = null
  if (type == 'pre_ico') {
    var tx = await PreIcoContract.changeOraclizeGas(newGas, {from: DEPLOY_ADDR, gasPrice: program.gasPrice})
    gasSet = await PreIcoContract.ORACLIZE_GAS_LIMIT.call()
  } else if (type == 'ico') {
    var tx = await IcoContract.changeOraclizeGas(newGas, {from: DEPLOY_ADDR, gasPrice: program.gasPrice})
    gasSet = await IcoContract.ORACLIZE_GAS_LIMIT.call()
  }
  assert.equal(gasSet.toNumber(), newGas)
  console.log('Done, tx hash: ' + tx.tx)
}

const changeOraclizeGasPrice = async (type) => {
  const newGasPrice = program.oraclizeGasPrice
  console.log('Changing Oraclize gas price to ' + newGasPrice + ' wei (' + w3u.fromWei(newGasPrice, 'gwei') + ' gwei)')
  var reply = await prompt.run()
  if (!yn(reply)) process.exit(1)
  if (type == 'pre_ico') {
    var tx = await PreIcoContract.changeOraclizeGasPrice(newGasPrice, {from: DEPLOY_ADDR, gasPrice: program.gasPrice})
  } else if (type == 'ico') {
    var tx = await IcoContract.changeOraclizeGasPrice(newGasPrice, {from: DEPLOY_ADDR, gasPrice: program.gasPrice})
  }
  console.log('Done, tx hash: ' + tx.tx)
}

const addReservation = async (address) => {
  console.log('Adding new reservation contract with address: ' + address)
  var instance = await RcContract.at(address)
  var start = await instance.preIcoStart.call()
  var end = await instance.preIcoEnd.call()
  console.log('Reservation contract start and end:')
  console.log('  start ' + new Date(start * 1000).toString())
  console.log('  end ' + new Date(end * 1000).toString())

  var reply = await prompt.run()
  if (!yn(reply)) process.exit(1)
  var tx = await PreIcoContract.addNewReservContract(address, {from: DEPLOY_ADDR})
  var addressCheck = await PreIcoContract.reservationContracts.call(address)
  assert.equal(addressCheck, true)
  console.log('Done, tx hash: ' + tx.tx)
}

const removeReservation = async (address) => {
  console.log('Removing reservation contract with address: ' + address)

  var reply = await prompt.run()
  if (!yn(reply)) process.exit(1)

  var tx = await PreIcoContract.removeReservContract(address, {from: DEPLOY_ADDR})
  var addressCheck = await PreIcoContract.reservationContracts.call(address)
  assert.equal(addressCheck, false)
  console.log('Done, tx hash: ' + tx.tx)
}

const changeStage = async (type, stage) => {
  console.log('Changing stage to ' + program.setStage + ' (value: ' + stage + ')')
  var reply = await prompt.run()
  if (!yn(reply)) process.exit(1)
  var stageNew = null
  if (type == 'pre_ico') {
    var tx = await PreIcoContract.changeStage(stage, {from: DEPLOY_ADDR, gasPrice: program.gasPrice})
    stageNew = await PreIcoContract.currentStage.call()
  } else if (type == 'ico') {
    var tx = await IcoContract.changeStage(stage, {from: DEPLOY_ADDR, gasPrice: program.gasPrice})
    stageNew = await IcoContract.currentStage.call()
  }
  assert.equal(stageNew.toNumber(), stage)
  console.log('Done, tx hash: ' + tx.tx)
}

const requestRateUpdate = async (type) => {
  console.log('Requesting Oraclize update 1 minute from now...')
  var reply = await prompt.run()
  if (!yn(reply)) process.exit(1)
  if (type == 'pre_ico') {
    var tx = await PreIcoContract.forceOraclizeUpdate(60, {from: DEPLOY_ADDR, gasPrice: program.gasPrice})
  } else if (type == 'ico') {
    var tx = await IcoContract.forceOraclizeUpdate(60, {from: DEPLOY_ADDR, gasPrice: program.gasPrice})
  }
  console.log('Done, tx hash: ' + tx.tx)
}

const updateRate = async (type) => {
  console.log('Getting ETH/USD rate from coin compare API...')
  var url = 'https://min-api.cryptocompare.com/data/price?fsym=ETH&tsyms=USD&sign=true'
  var result = await request({uri: url, method: 'GET', resolveWithFullResponse: true})
  var rate = JSON.parse(result.body).USD
  console.log('Rate from ' + url)
  console.log(rate + ' ETH/USD')
  const rateToSet = parseInt(rate)
  var signData = result.headers.authorization + '#' + result.body
  console.log(signData)
  console.log(`Updating rate to: ${rateToSet}`)
  var reply = await prompt.run()
  if (!yn(reply)) process.exit(1)
  var rateFile = 'data/rate_update_' + parseInt(Date.now()/1000)
  fs.writeFileSync(rateFile, signData)
  console.log('saved to ' + rateFile)

  var newRate = 0
  if (type == 'pre_ico') {
    var tx = await PreIcoContract.updateEthUsdManually(rateToSet, {from: DEPLOY_ADDR, gasPrice: program.gasPrice})
    newRate = await PreIcoContract.ethUsd.call()
  } else if (type == 'ico') {
    var tx = await IcoContract.updateEthUsdManually(rateToSet, {from: DEPLOY_ADDR, gasPrice: program.gasPrice})
    newRate = await IcoContract.ethUsd.call()
  }
  assert.equal(newRate / 10 ** 18, rateToSet)
  console.log('Done, tx hash: ' + tx.tx)
}

const withdrawAll = async (type) => {
  console.log('Withdrawing balance... (requires contract balance > 0 and stage finalized)')
  var reply = await prompt.run()
  if (!yn(reply)) process.exit(1)
  if (type == 'pre_ico') {
    var tx = await PreIcoContract.withdrawBalance({from: DEPLOY_ADDR, gasPrice: program.gasPrice})
  } else if (type == 'ico') {
    var tx = await IcoContract.withdrawBalance({from: DEPLOY_ADDR, gasPrice: program.gasPrice})
  }
  console.log('Done, tx hash: ' + tx.tx)
}

const topUpBalance = async (type) => {
  console.log('Top up ' + type + ' balance, with ' + program.value + ' wei (' + w3u.fromWei(program.value, 'ether') + ' ETH)')
  // we need running stage
  var reply = await prompt.run()
  if (!yn(reply)) process.exit(1)
  var verifyCode = 28391728448
  if (type == 'pre_ico') {
    var nowStage = await PreIcoContract.currentStage.call()
    assert.equal(nowStage, 2, 'Top up balance requires Running stage')
    var tx = await PreIcoContract.topUpBalance(
      verifyCode,
      {
        from: DEPLOY_ADDR,
        gasPrice: program.gasPrice,
        value: program.value
      })
  } else if (type == 'ico') {
    var nowStage = await IcoContract.currentStage.call()
    assert.equal(nowStage, 2, 'Top up balance requires Running stage')
    var tx = await IcoContract.topUpBalance(
      verifyCode,
      {
        from: DEPLOY_ADDR,
        gasPrice: program.gasPrice,
        value: program.value
      })
  }
  console.log('Done, tx hash: ' + tx.tx)
}

const getBalancesAndSave = async (type, buyersTemp) => {
  if (buyersTemp.length == 0) {
    console.log('No buyers found, exiting...')
    process.exit(0)
  }
  const buyers = _.uniq(buyersTemp)
  var balanceList = []
  for (var i = 0; i < buyers.length; i++) {
    if (type == 'pre_ico') {
      var balanceToken = await PreIcoContract.getInvestorBalance.call(buyers[i])
      var investWei = await PreIcoContract.getInvestorWeiBalance.call(buyers[i])
    } else if (type == 'ico') {
      var balanceToken = await IcoContract.getInvestorBalance.call(buyers[i])
      var investWei = await IcoContract.getInvestorWeiBalance.call(buyers[i])
    }
    balanceList.push({address: buyers[i], token: balanceToken.toNumber(), wei: investWei.toNumber()})
  }
  var fields = ['address', 'token', 'wei']
  var csv = json2csv({ data: balanceList, fields: fields })
  var timeStr = new Date().toString().replace(/ /g, '_')

  var fileNameBalanceCsv = 'data/' + type + '__balances__' + timeStr + '.csv'
  fs.writeFileSync(fileNameBalanceCsv, csv)
  var fileNameBalanceJson = 'data/' + type + '__balances__' + timeStr + '.json'
  fs.writeFileSync(fileNameBalanceJson, JSON.stringify(balanceList))

  var filenameInvestors = 'data/' + type + '__investors__' + timeStr + '.json'
  fs.writeFileSync(filenameInvestors, JSON.stringify(buyers))

  console.log('Saved a total of ' + buyers.length + ' balances/addresses to:')
  console.log('    ' + fileNameBalanceCsv)
  console.log('    ' + fileNameBalanceJson)
  console.log('    ' + filenameInvestors)
  process.exit(0)
}

const exportAll = async (type) => {
  console.log('Exporting ' + type + ' buyers contract states at this moment')
  var reply = await prompt.run()
  if (!yn(reply)) process.exit(1)
  var buyersList = []
  if (type == 'pre_ico') {
    await PreIcoContract.TokenPurchase({}, { fromBlock: 0, toBlock: 'latest' }).get((error, eventResult) => {
      if (error) console.error(error)
      for (var i = 0; i < eventResult.length; i++) {
        var ev = eventResult[i]
        var args = ev.args
        buyersList.push(args.beneficiary)
      }
      return getBalancesAndSave('pre_ico', buyersList)
    })
  } else if (type == 'ico') {
    await IcoContract.TokenPurchase({}, { fromBlock: 0, toBlock: 'latest' }).get((error, eventResult) => {
      if (error) console.error(error)
      for (var i = 0; i < eventResult.length; i++) {
        var ev = eventResult[i]
        var args = ev.args
        buyersList.push(args.beneficiary)
      }
      return getBalancesAndSave('ico', buyersList)
    })
  }
}

const whiteList = async (type) => {
  console.log('Whitelisting from file: ' + program.whitelist)

  var addressesLoaded = JSON.parse(fs.readFileSync(program.whitelist).toString())
  if (addressesLoaded.length == 0) {
    console.log('No address to whitelist exiting...')
    process.exit(1)
  }

  const toWhitelist =  _.uniq(addressesLoaded)

  if (toWhitelist.length !== addressesLoaded.length) {
    console.log('Found a missmatch, addresses to whitelist contains duplicates, exiting...')
    process.exit(1)
  }

  console.log('Addresses to whitelist: ' + toWhitelist.length)
  if (toWhitelist.length < 50) console.log(toWhitelist+'\n')

  var reply = await prompt.run()
  if (!yn(reply)) process.exit(1)

  // chunk array in 100
  var whitelistChunks = _.chunk(toWhitelist, 100)

  if (type == 'pre_ico') {
    for (var i = 0; i < whitelistChunks.length; i++) {
      console.log('Whitelisting ' + whitelistChunks[i])
      var txData = '0x' + abi.simpleEncode('addWhitelistAddrByList(address[])', whitelistChunks[i]).toString('hex')
      var tx = await PreIcoContract.sendTransaction(
        {from: DEPLOY_ADDR, gasPrice: program.gasPrice, data: txData}
      )
      console.log('Whitelist for ' + whitelistChunks[i] + ' done, tx hash: ' + tx.tx)
    }
  } else if (type == 'ico') {
    for (var i = 0; i < whitelistChunks.length; i++) {
      console.log('Whitelisting ' + whitelistChunks[i])
      var txData = '0x' + abi.simpleEncode('addWhitelistAddrByList(address[])', whitelistChunks[i]).toString('hex')
      var tx = await IcoContract.sendTransaction(
        {from: DEPLOY_ADDR, gasPrice: program.gasPrice, data: txData}
      )
      console.log('Whitelist for ' + whitelistChunks[i] + ' done, tx hash: ' + tx.tx)
    }
  }
  // check if was set
  for (var i = 0; i < toWhitelist.length; i++) {
    var addr = toWhitelist[i]
    var isWhitelisted = false
    if (type == 'pre_ico') {
      isWhitelisted = await PreIcoContract.isWhitelisted.call(addr)
    } else if (type == 'ico') {
      isWhitelisted = await IcoContract.isWhitelisted.call(addr)
    }
    assert.equal(isWhitelisted, true)
  }
  console.log('Done')
}

const finalizeList = async (type) => {
  console.log('Finalizing from file: ' + program.finalize)

  var addressesLoaded = JSON.parse(fs.readFileSync(program.finalize).toString())
  if (addressesLoaded.length == 0) {
    console.log('No address to finalize exiting...')
    process.exit(1)
  }

  const toFinalize =  _.uniq(addressesLoaded)

  if (toFinalize.length !== addressesLoaded.length) {
    console.log('Found a missmatch, addresses to finalize contains duplicates, exiting...')
    process.exit(1)
  }

  console.log('Addresses to finalize: ' + toFinalize.length)
  if (toFinalize.length < 50) console.log(toFinalize+'\n')

  var reply = await prompt.run()
  if (!yn(reply)) process.exit(1)

  // chunk array in 100
  var finalizeChunks = _.chunk(toFinalize, 100)

  if (type == 'pre_ico') {
    for (var i = 0; i < finalizeChunks.length; i++) {
      console.log('Finalizing ' + finalizeChunks[i])
      var txData = '0x' + abi.simpleEncode('finalizeInvestorsByList(address[])', finalizeChunks[i]).toString('hex')
      var tx = await PreIcoContract.sendTransaction(
        {from: DEPLOY_ADDR, gasPrice: program.gasPrice, data: txData}
      )
      console.log('Finalize for ' + finalizeChunks[i] + ' done, tx hash: ' + tx.tx)
    }
  } else if (type == 'ico') {
    for (var i = 0; i < finalizeChunks.length; i++) {
      console.log('Finalizing ' + finalizeChunks[i])
      var txData = '0x' + abi.simpleEncode('finalizeInvestorsByList(address[])', finalizeChunks[i]).toString('hex')
      var tx = await IcoContract.sendTransaction(
        {from: DEPLOY_ADDR, gasPrice: program.gasPrice, data: txData}
      )
      console.log('Finalize for ' + finalizeChunks[i] + ' done, tx hash: ' + tx.tx)
    }
  }

  console.log('Done')
}

const burnUnsold = async (type) => {
  console.log('Burning remaining tokens...')


  var tokensToBurn = null

  if (type == 'pre_ico') {
    var tokensFinalized = await PreIcoContract.tokensFinalized.call()
    var maxTokens = await PreIcoContract.MAX_TOKENS.call()
    tokensToBurn = maxTokens.toNumber() - tokensFinalized.toNumber()
  } else if (type == 'ico') {
    var tokensFinalized = await IcoContract.tokensFinalized.call()
    var maxTokens = await IcoContract.MAX_TOKENS.call()
    tokensToBurn = maxTokens.toNumber() - tokensFinalized.toNumber()
  }

  console.log(`${tokensToBurn} sphi to burn (${tokensToBurn/10**18} PHI)`)

  var reply = await prompt.run()
  if (!yn(reply)) process.exit(1)
  if (type == 'pre_ico') {
    var tx = await PreIcoContract.burnRemainingTokens(
      {from: DEPLOY_ADDR, gasPrice: program.gasPrice, value: program.value}
    )
  } else if (type == 'ico') {
    var tx = await IcoContract.burnRemainingTokens(
      {from: DEPLOY_ADDR, gasPrice: program.gasPrice, value: program.value}
    )
  }

  console.log('Done, tx hash: ' + tx.tx)
}

const verify = async (type) => {
  console.log('Verifying ' + type + ' buyers')
  console.log('Balances: ' + program.balances)
  console.log('Whitelist addresses: ' + program.whitelist)
  var whitelistAddresses = JSON.parse(fs.readFileSync(program.whitelist).toString())
  var balances = JSON.parse(fs.readFileSync(program.balances).toString())
  if (whitelistAddresses.length == 0 || balances.length == 0) {
    console.log('No whitelist or balances to check')
    process.exit(1)
  }
  if (whitelistAddresses.length != balances.length) {
    console.log('Found a missmatch between the length of the whitelist addresses and balances')
    console.log('whitelist: ' + whitelistAddresses.length + ' - balances: ' + balances.length)
    process.exit(1)
  }
  
  console.log('')
  
  for (var i = 0; i < balances.length; i++) {
    if (whitelistAddresses.indexOf(balances[i].address) > -1) {
      if (program.finalized == true && balances[i].token > 0) {
        console.log(balances[i].address + ' not finalized')
      }
      if (program.finalized == false && (balances[i].wei == 0 || balances[i].token == 0)) {
        console.log(balances[i].address + ' has wei or token balance = 0')
      }
    }
  }
  
  console.log('')
  
  console.log('Done')
}

const initializeCrowdsale = async (type) => {
  console.log('Initializing ' + type + ' with ' + program.value + ' wei (' + w3u.fromWei(program.value, 'ether') + ' ETH)')
  var thisMode = config[type]
  var startTime = thisMode.start
  var endTime = thisMode.end
  var tokenAddr = config.token.address
  var intervalUpdate = config.interval_update

  console.log(`  startTime: ${new Date(startTime*1000).toString()}`)
  console.log(`  endTime: ${new Date(endTime*1000).toString()}`)
  console.log(`  token addr: ${tokenAddr}`)
  console.log(`  interval update: ${intervalUpdate/60} minutes`)

  var reply = await prompt.run()
  if (!yn(reply)) process.exit(1)
  if (type == 'pre_ico') {
    var tx = await PreIcoContract.initializeCrowdsale(
      startTime,
      endTime,
      tokenAddr,
      intervalUpdate,
      {from: DEPLOY_ADDR, gasPrice: program.gasPrice, value: program.value}
    )
  } else if (type == 'ico') {
    var tx = await IcoContract.initializeCrowdsale(
      startTime,
      endTime,
      tokenAddr,
      intervalUpdate,
      {from: DEPLOY_ADDR, gasPrice: program.gasPrice, value: program.value}
    )
  }
  var info = await getInfo(type)
  assert.equal(startTime, info.startTime)
  assert.equal(endTime, info.endTime)
  assert.equal(tokenAddr, info.token)
  assert.equal(1, info.stage)
  console.log('Done, tx hash: ' + tx.tx)
}

const getInfo = async (type) => {
  var info = {}
  if (type == 'pre_ico') {
    var start = await PreIcoContract.startTime.call()
    var end = await PreIcoContract.endTime.call()
    var tokenAddr = await PreIcoContract.token.call()
    var intervalUpdate = await PreIcoContract.intervalUpdate.call()
    var stage = await PreIcoContract.currentStage.call()
  } else if (type == 'ico') {
    var start = await IcoContract.startTime.call()
    var end = await IcoContract.endTime.call()
    var tokenAddr = await IcoContract.token.call()
    var intervalUpdate = await IcoContract.intervalUpdate.call()
    var stage = await IcoContract.currentStage.call()
  }
  info.startTime = start.toNumber()
  info.endTime = end.toNumber()
  info.token = tokenAddr
  info.intervalUpdate = intervalUpdate.toNumber()
  info.stage = stage.toNumber()
  if (program.info) {
    console.log('Current stage: ' + info.stage)
    console.log('start: ' + new Date(info.startTime * 1000).toString())
    console.log('end: ' + new Date(info.endTime * 1000).toString())
    console.log('token address: ' + info.token)
    console.log('interval update (secs): ' + info.intervalUpdate)
  }
  return info
}

var stageToSet = undefined
if (program.setStage) {
  /*
    ToInitialize, // [0] has not been initialized
    Waiting,      // [1] is waiting start time
    Running,      // [2] is running (between start time and end time)
    Paused,       // [3] has been paused
    Finished,     // [4] has been finished (but not finalized)
    Finalized     // [5] has been finalized
  */
  if (program.setStage.toLowerCase() == 'waiting') {
    stageToSet = 1
  }
  if (program.setStage.toLowerCase() == 'running') {
    stageToSet = 2
  }
  if (program.setStage.toLowerCase() == 'paused') {
    stageToSet = 3
  }
  if (program.setStage.toLowerCase() == 'finished') {
    stageToSet = 4
  }
  if (program.setStage.toLowerCase() == 'finalized') {
    stageToSet = 5
  }
}

async function main () {
  var type = null

  if (program.preIco) type = 'pre_ico'
  if (program.ico) type = 'ico'

  if (type == null) {
    console.log('Pleae choose contract mode, --ico or --pre-ico')
    process.exit(0)
  }

  if (type == 'pre_ico') IcoContract = null
  if (type == 'ico') PreIcoContract = null
  
  console.log('Interacting with ' + type + ' contract')

  if (program.deploy) {
    return await deployContract(type)
  }

  // load addr
  await updateContractAddress(type)

  if (program.info) return await getInfo(type)

  if (program.initialize) {
    if (!program.value || program.value == 0) throw new Error('Requires --value')
    await initializeCrowdsale(type)
  }

  if (type == 'pre_ico' && program.addReservation) {
    await addReservation(program.addReservation)
  }

  if (type == 'pre_ico' && program.removeReservation) {
    await removeReservation(program.removeReservation)
  }

  if (stageToSet) {
    await changeStage(type, stageToSet)
  }

  if (program.topUp) {
    if (!program.value || program.value == 0) throw new Error('Requires --value')
    await topUpBalance(type)
  }

  if (program.export) {
    await exportAll(type)
  }

  if (program.whitelist && !program.verify) {
    await whiteList(type)
  }

  if (program.finalize) {
    await finalizeList(type)
  }

  if (program.withdraw) {
    await withdrawAll(type)
  }

  if (program.burnUnsold) {
    await burnUnsold(type)
  }

  if (program.updateRate) {
    await updateRate(type)
  }

  if (program.requestRateUpdate) {
    await requestRateUpdate(type)
  }

  if (program.oraclizeGasLimit) {
    await changeOraclizeGasLimit(type)
  }

  if (program.oraclizeGasPrice) {
    await changeOraclizeGasPrice(type)
  }

  if (program.verify) {
    if (!program.balances || !program.whitelist) {
      console.log('Requires --balances file path and --whitelist file path')
      process.exit(0)
    }
    if (program.finalized) console.log('Checking finalized addresses')
    verify(type)
  }
}

main()
