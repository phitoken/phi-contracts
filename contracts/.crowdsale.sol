pragma solidity ^0.4.18;

import "./SafeMath.sol";
// Import the token interface from the `token` folder
import "./Token.sol";
// Import Oraclize contract
import "./usingOraclize.sol";

/**
 * @title Crowdsale
 * @dev Crowdsale is a base contract for managing a token crowdsale.
 * Crowdsales have a start and end timestamps, where investors can make
 * token purchases and the crowdsale will assign them tokens based
 * on a token per ETH rate. Funds collected are forwarded to a wallet
 * as they arrive. The contract mustBeAtStage a `Token` that will be
 * transfered as contributions arrive, note that the crowdsale contract
 * must be owner of the token in order to be able to transfer it.
 */
contract Crowdsale is usingOraclize {
  using SafeMath for uint256;

  /**
   * Section 1
   * - Variables
   */
  /* Section 1.1 crowdsale key variables */
  // The token being sold
  Token public token;

  // start and end timestamps where investments are allowed (both inclusive)
  uint256 public startTime;
  uint256 public endTime;

  // address where ETH funds are sent
  address public wallet;

  // owner of this contract
  address public owner;

  // How many PHI tokens to sell (in sphi)
  uint256 public MAX_TOKENS = 3524578 * 10**18;
  // Keep track of sold tokens
  uint256 public tokensSold = 0;
  // Keep track of tokens sent to whitelisted addresses
  uint256 public tokensFinalized = 0;

  /* Section 1.2 rate/price variables */
  // ETH/USD rate
  uint256 public ethUsd;
  /**
    * Phi rate in USD multiplied by 10**18
    * because of Solidity float limitations,
    * see http://solidity.readthedocs.io/en/v0.4.19/types.html?highlight=Fixed%20Point%20Numbers#fixed-point-numbers
    */
  uint256 public phiRate = 1278246852100000000; // pre-ico fixed price (1,61803399 * 21%) * 10**18

  /* Section 1.3 oracle related variables */
  // keep track of the last ETH/USD update
  uint256 public lastOracleUpdate;
  // set default ETH/USD update interval (in seconds)
  uint256 public intervalUpdate;
  // custom oraclize_query gas cost (wei), expected gas usage is ~70k
  uint256 public ORACLIZE_GAS_LIMIT = 85000;

  /* Section 1.4 variables to keep KYC and balance state */
  // amount of raised money in wei
  uint256 public weiRaised;
  // keep track of addresses that are allowed to keep tokens
  address[] public whitelistAddr;
  // keep track of investors
  mapping(address => uint256) public balances;


  /**
   * Section 2
   * - Enums
   */
  // Describes current crowdsale stage
  enum Stage
  {
    ToInitialize, // [0] Crowdsale has not been initialized
    Waiting,      // [1] Crowdsale is waiting start time
    Running,      // [2] Crowdsale is running (between start time and end time)
    Paused,       // [3] Crowdsale has been paused
    Finished,     // [4] Crowdsale has been finished (but not finalized)
    Finalized     // [5] Crowdsale has been finalized
  }
  Stage public currentStage = Stage.ToInitialize;
  // TODO: maybe change current stage to const function

  /**
   * Section 3
   * - Events
   */
  /**
   * event for token purchase logging
   * @param purchaser who paid for the tokens
   * @param beneficiary who got the tokens
   * @param value weis paid for purchase
   * @param amount amount of tokens purchased
   */
  event TokenPurchase(address indexed purchaser, address indexed beneficiary, uint256 value, uint256 amount);

  /**
   * event to emit when a new rate is received from Oraclize
   * @param newRate Rate received in WEI
   * @param timeRecv When the rate was received
   */
  event LogRateUpdate(uint newRate, uint timeRecv);

  /**
   * event to emit in case the contract needs balance (for Oraclize queries)
   */
  event LogBalanceRequired();

  /**
   * event to notify initialize
   */
   event LogCrowdsaleInit();

  /**
   * Section 4
   * - Modifiers
   */
  /*
    Check if a purchase can be made, check startTime, endTime
    and update Stage if so
  */
  modifier validPurchase {
    bool withinPeriod = now >= startTime && now <= endTime;
    require(msg.value != 0 && withinPeriod && startTime != 0);
    /*
      Update current stage only if the previous stage is `Waiting`
      and we are within the crowdsale period, used to automatically
      update the stage by an investor
    */
    if (withinPeriod == true && currentStage == Stage.Waiting) {
      currentStage = Stage.Running;
    }
    _;
  }
  // Allow only the owner of this contract
  modifier onlyOwner {
    require(msg.sender == owner);
    _;
  }

  // Allow only if is close to start time or if is it running
  modifier closeOrDuringCrowdsale {
    require(now >= startTime - intervalUpdate);
    require(currentStage == Stage.Running || currentStage == Stage.Waiting);
    _;
  }

  // Check if the provided stage is the current one
  modifier mustBeAtStage (Stage stageNeeded) {
    require(stageNeeded == currentStage);
    _;
  }

  /**
   * @dev Constructor
   * @param _wallet Where to send ETH collected from the Crowdsale
   */
  function Crowdsale(address _wallet) public {
    require(phiRate > 0 && startTime == 0);
    require(_wallet != address(0));

    // update global variable
    wallet = _wallet;
    owner = msg.sender;

    currentStage = Stage.ToInitialize;

    // Set the Oraclize proof type
    oraclize_setProof(proofType_TLSNotary | proofStorage_IPFS);
  }

  /**
   * @dev Used to init crowdsale, set start-end time, start usd rate update
   * @notice You must send some ETH to cover the oraclize_query fees
   * @param _startTime Start of the crowdsale (UNIX timestamp)
   * @param _endTime End of the crowdsale (UNIX timestamp)
   * @param _token Address of the PHI ERC223 Token
   */
  function initializeCrowdsale(
    uint256 _startTime,
    uint256 _endTime,
    address _token,
    uint256 _intervalUpdate
  )
    public
    payable
    onlyOwner
    mustBeAtStage(Stage.ToInitialize)
  {
    require(_startTime >= now);
    require(_endTime >= _startTime);
    require(_token != address(0));
    require(msg.value > 0);

    // interval update must be above or equal to 5 seconds
    require(_intervalUpdate >= 5);

    // update global variables
    startTime = _startTime;
    endTime = _endTime;
    token = Token(_token);
    intervalUpdate = _intervalUpdate;

    // update stage
    currentStage = Stage.Waiting;

    /*
      start to fetch ETH/USD price `intervalUpdate` before the `startTime`,
      30 seconds is added because Oraclize takes time to call the __callback
    */
    updateEthRateWithDelay(startTime - (intervalUpdate + 30));

    LogCrowdsaleInit();

    // check amount of tokens held by this contract matches MAX_TOKENS
    assert(token.balanceOf(address(this)) == MAX_TOKENS);
  }

  // TODO: allow to pause crowdsale

  /* Oraclize related functions */
  /**
   * @dev Callback function used by Oraclize to update the price.
   * @notice ETH/USD rate is receivd and converted to wei, this
   * functions is used also to automatically update the stage status
   * @param myid Unique identifier for Oraclize queries
   * @param result Result of the requested query
   * @param proof IPFS hash of the TLSNotary proof
   */
  function __callback(
    bytes32 myid,
    string result,
    bytes proof
  ) 
    public
    closeOrDuringCrowdsale
  {
    if (msg.sender != oraclize_cbAddress()) throw;
    // parse to int and multiply by 10**18 to allow math operations
    uint256 usdRate = parseInt(result).mul(10**18);
    // do not allow 0
    require(usdRate > 0);

    ethUsd = usdRate;
    LogRateUpdate(ethUsd, now);

    // check if time is over
    if (hasEnded() == true) {
     currentStage = Stage.Finished;
    }
    
    updateEthRate();
  }

  /**
   * @dev Used to recursively call the oraclize query 
   * @notice This function will not throw in case the
   * interval update is exceeded, in this way the latest
   * update made to the ETH/USD rate is kept 
   */
  function updateEthRate () internal {
    // prevent multiple updates
    if((now - lastOracleUpdate) >= intervalUpdate || currentStage != Stage.Running) {}
    else {
      updateEthRateWithDelay(intervalUpdate);
    }
  }

  /**
   * @dev Change interval update
   * @param newInterval New interval rate (in seconds)
   */
  function changeIntervalUpdate (uint newInterval) public onlyOwner {
    require(newInterval >= 5);
    intervalUpdate = newInterval;
  }

  /**
   * @dev Helper function around oraclize_query
   * @notice Call oraclize_query with a delay in seconds
   * @param delay Delay in seconds 
   */
  function updateEthRateWithDelay (uint delay) internal {
    // delay cannot be below 5 seconds (too fast)
    require(delay >= 5);
    if (oraclize_getPrice("URL", ORACLIZE_GAS_LIMIT) > this.balance) {
      // Notify that we need a top up 
      LogBalanceRequired();
    } else {
        // Get ETH/USD rate from kraken API
        oraclize_query(
          delay,
          "URL",
          "json(https://api.kraken.com/0/public/Ticker?pair=ETHUSD).result.XETHZUSD.c.0",
          ORACLIZE_GAS_LIMIT
        );
    }
  }

  /**
   * @dev Allow owner to force rate update
   * @param delay Delay in seconds of oraclize_query, can be set to 5 (minimum)
   */
  function forceOraclizeUpdate (uint256 delay) public onlyOwner {
    updateEthRateWithDelay(delay);
  }

  /**
   * @dev Change Oraclize gas limit (used in oraclize_query)
   * @notice To be used in case the default gas cost is too low
   * @param newGas New gas to use (in wei)
   */
  function changeOraclizeGas(uint newGas) public onlyOwner {
    require(newGas > 0 && newGas <= 4000000);
    ORACLIZE_GAS_LIMIT = newGas;
  }

  // TODO test
  /**
   * @dev Top up balance
   * @notice This function must be used **only if 
   * this contract balance is too low for oraclize_query
   * to be executed**
   * @param verifyCode Used only to allow people that read
   * the notice (not accidental)
   */
  function topUpBalance (uint verifyCode) public payable {
    // value is required
    require(msg.value > 0);
    // make sure only the people that read
    // this can use the function
    require(verifyCode == 28391728448);
  }

  // TODO test
  /**
   * @dev Withdraw balance of this contract to the `wallet` address
   * @notice Used only if there are some leftover
   * funds (because of topUpBalance)
  */
  function withdrawBalance() public mustBeAtStage(Stage.Finalized) {
    wallet.transfer(this.balance);
  }

  /* Invest related functions */
  /**
   * @dev Fallback function, to be used to purchase tokens
   */
  function () external payable {
    buyTokens(msg.sender);
  }

  /**
   * @dev Low level function to purchase function on behalf of a beneficiary
   * @notice If you call directly this function your are buying for someone else
   * @param beneficiary Where tokens should be sent
   */
  function buyTokens(address beneficiary) public validPurchase mustBeAtStage(Stage.Running) payable {
    require(beneficiary != address(0));
    require(beneficiary != address(this));

    uint256 weiAmount = msg.value;

    // calculate token amount to be transfered
    uint256 tokens = getTokenAmount(weiAmount);

    // check if we are below MAX_TOKENS (and revert if throw will be exceeded)
    assert(tokensSold.add(tokens) <= MAX_TOKENS);

    // update tokens sold
    tokensSold = tokensSold.add(tokens);

    // update total wei counter
    weiRaised = weiRaised.add(weiAmount);

    // update balance of the beneficiary
    balances[beneficiary] = balances[beneficiary].add(tokens);

    TokenPurchase(msg.sender, beneficiary, weiAmount, tokens);

    // forward ETH
    forwardFunds();
  }

  /**
   * @dev send ether to the fund collection wallet
   */
  function forwardFunds() internal {
    wallet.transfer(msg.value);
  }

  /* Finalize/whitelist functions */
  /**
   * @dev Add multiple whitelisted addresses
   * @notice Must be called after the crowdsale has finished
   * @param investors Array or investors to enable
   */ 
  function addWhitelistAddrs (address[] investors) public onlyOwner mustBeAtStage(Stage.Finished) {
    require(hasEnded() == true);
    for(uint256 i = 0; i < investors.length; i++) {
      whitelistSingleAddress(investors[i]);
    }
  }
  // TODO test
  /**
   * @dev Whitelist a specific address
   * @notice This is mainly an helper function but can be useful in
   * case the `addWhitelistAddrs` loop has issues
   * @param investor Investor to whitelist
   */ 
  function whitelistSingleAddress (address investor) public onlyOwner mustBeAtStage(Stage.Finished) {
    require(investor != address(0) && investor != address(this));
    whitelistAddr.push(investor);
  }
  // TODO test
  /**
   * @dev Remove an address from whitelist
   * @notice Can be useful in case an address is blocking the loop 
   */
  function removeWhitelistedAddress (address toRemove) public onlyOwner mustBeAtStage(Stage.Finished) {
    for(uint256 i = 0; i < whitelistAddr.length; i++) {
      if(whitelistAddr[i] == toRemove) {
        delete whitelistAddr[i];
      }
    }
  }

  /**
   * @dev Finalize crowdsale
   * @notice Transfers tokens to whitelisted address
   */
  function finalize() public onlyOwner mustBeAtStage(Stage.Finished) {
    require(hasEnded() == true);
    // loop over the whitelisted addresses
    for(uint256 i = 0; i < whitelistAddr.length; i++) {
      // save data into variables
      address whitelisted = whitelistAddr[i];
      // remove whitelist
      delete whitelistAddr[i];

      finalizeSingleInvestor(whitelisted);
    }
  }

  // TODO test
  /**
   * @dev Finalize a specific investor
   * @notice This is mainly an helper function to `finalize` but
   * can be used if `finalize` has issues with the loop
   * @param investorAddr Address to finalize
   */
  function finalizeSingleInvestor(address investorAddr) public onlyOwner mustBeAtStage(Stage.Finished) {
    require(investorAddr != address(0) && investorAddr != address(this));
    require(balances[investorAddr] > 0);
    // save data into variables
    uint256 balanceToTransfer = balances[investorAddr];
    // reset current state
    balances[investorAddr] = 0;
    // transfer token to the investor address and the balance
    // that we have recorded before
    // TODO: check if this is the best way to send tokens with gas cost
    token.transfer(investorAddr, balanceToTransfer);
    // update tokens sent
    tokensFinalized = tokensFinalized.add(balanceToTransfer);
    assert(tokensFinalized <= MAX_TOKENS);
  }

  /**
   * @dev Burn unsold tokens
   * @notice Call this function after finalizing
   */
  function burnRemainingTokens() public onlyOwner mustBeAtStage(Stage.Finalized) {
    // should always be true, if not there is an issue with the crowdsale
    require(MAX_TOKENS >= tokensFinalized);
    uint unsold = MAX_TOKENS.sub(tokensFinalized);
    if (unsold > 0) {
      token.burn(unsold);
    }
  }

  /**
   * @dev Allow to change the current stage
   * @param newStage New stage
   */
  function changeStage (Stage newStage) public onlyOwner {
    currentStage = newStage;
  }

  /**
   * @dev Crowdsale status (based only on time)
   * @return true if crowdsale event has ended
   */
  function hasEnded() public constant returns (bool) {
    return now > endTime && startTime != 0;
  }

  /* Price functions */
  /**
   * @dev Get current ETH/PHI rate (1 ETH = getEthPhiRate() PHI)
   * @notice It divides (ETH/USD rate) / (PHI/USD rate), use the
   * custom function `getEthPhiRate(false)` if you want a more
   * accurate rate
   * @return ETH/PHI rate
   */
  function getEthPhiRate () public constant returns (uint) {
    // 1/(ETH/PHI rate) * (ETH/USD rate) should return PHI rate
    // multiply by 1000 to keep decimals from the division
    return ethUsd.div(phiRate);
  }
  /**
   * @dev Get current kETH/PHI rate (1000 ETH = getkEthPhiRate() PHI)
   * used to get a more accurate rate (by not truncating decimals)
   * @notice It divides (ETH/USD rate) / (PHI/USD rate)
   * @return kETH/PHI rate
   */
  function getkEthPhiRate () public constant returns (uint) {
    // 1/(kETH/PHI rate) * (ETH/USD rate) should return PHI rate
    // multiply by 1000 to keep decimals from the division, and return kEth/PHI rate
    return ethUsd.mul(1000).div(phiRate);
  }

  /**
   * @dev Calculate amount of token based on wei amount
   * @param weiAmount Amount of wei
   * @return Amount of PHI tokens
   */
  function getTokenAmount(uint256 weiAmount) public constant returns(uint256) {
    // get kEth rate to keep decimals
    uint currentKethRate = getkEthPhiRate();
    // divide by 1000 to revert back the multiply
    return currentKethRate.mul(weiAmount).div(1000);
  }

  // TODO: set finalize stage on finalize call

  /* Helper functions for token balance */
  /**
   * @dev Returns how many tokens an investor has
   * @param investor Investor to look for
   * @return Balance of the investor
   */
  function getInvestorBalance(address investor) external constant returns (uint) {
    return balances[investor];
  }

  /**
   * @dev Returns the whitelisted address at the provided index
   * @param index Index to look for
   * @return Address at the provided index
   */
  function getWhitelistedAddrAt(uint256 index) external constant returns (address) {
    return whitelistAddr[index];
  }

  // TODO: add RC contracts logic to allow only some addresses

}
