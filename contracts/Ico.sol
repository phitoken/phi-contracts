pragma solidity ^0.4.18;

import "./SafeMath.sol";
// Import the token interface from the `token` folder
import "./Token.sol";
// Import Oraclize contract
import "./usingOraclize.sol";

/**
 * @title Ico
 * @dev Ico is a base contract for managing a token crowdsale.
 * Crowdsales
 */
contract Ico is usingOraclize {
  using SafeMath for uint256;

  /**
   * Section 1
   * - Variables
   */
  /* Section 1.1 crowdsale key variables */
  // The token being sold
  Token public token;

  // start and end timestamps where investments are allowed (both inclusive)
  uint256 public startTime;
  uint256 public endTime;

  // address where ETH funds are sent
  address public wallet;

  // owner of this contract
  address public owner;
  // How many PHI tokens to sell (in sphi)
  uint256 public MAX_TOKENS = 7881196 * 10**18;
  // Keep track of sold tokens
  uint256 public tokensSold = 0;
  // Keep track of tokens sent to whitelisted addresses
  uint256 public tokensFinalized = 0;

  /* Section 1.2 rate/price variables */
  // ETH/USD rate
  uint256 public ethUsd;
  /**
    * Phi rate in USD multiplied by 10**18
    * because of Solidity float limitations,
    * see http://solidity.readthedocs.io/en/v0.4.19/types.html?highlight=Fixed%20Point%20Numbers#fixed-point-numbers
    */
  uint256 public phiRate = 1618033990000000000; // ico fixed price 1.61803399 * 10**18

  /* Section 1.3 oracle related variables */
  // keep track of the last ETH/USD update
  uint256 public lastOracleUpdate;
  // set default ETH/USD update interval (in seconds)
  uint256 public intervalUpdate;
  // custom oraclize_query gas cost (wei), expected gas usage is ~110k
  uint256 public ORACLIZE_GAS_LIMIT = 145000;

  /* Section 1.4 variables to keep KYC and balance state */
  // amount of raised money in wei
  uint256 public weiRaised;
  // keep track of addresses that are allowed to keep tokens
  mapping(address => bool) public isWhitelisted;
  // keep track of investors (token balance)
  mapping(address => uint256) public balancesToken;
  // keep track of invested amount (wei balance)
  mapping(address => uint256) public balancesWei;

  /**
   * Section 2
   * - Enums
   */
  // Describes current crowdsale stage
  enum Stage
  {
    ToInitialize, // [0] ico has not been initialized
    Waiting,      // [1] ico is waiting start time
    Running,      // [2] ico is running (between start time and end time)
    Paused,       // [3] ico has been paused
    Finished,     // [4] ico has been finished (but not finalized)
    Finalized     // [5] ico has been finalized
  }
  Stage public currentStage = Stage.ToInitialize;

  /**
   * Section 3
   * - Events
   */
  /**
   * event for token purchase logging
   * @param purchaser who paid for the tokens
   * @param beneficiary who got the tokens
   * @param value weis paid for purchase
   * @param amount amount of tokens purchased
   */
  event TokenPurchase(address indexed purchaser, address indexed beneficiary, uint256 value, uint256 amount);

  /**
   * event to emit when a new rate is received from Oraclize
   * @param newRate Rate received in WEI
   * @param timeRecv When the rate was received
   */
  event LogRateUpdate(uint newRate, uint timeRecv);

  /**
   * event to emit in case the contract needs balance (for Oraclize queries)
   */
  event LogBalanceRequired();

  /**
   * event to notify initialize
   */
   event LogCrowdsaleInit();

  /**
   * Section 4
   * - Modifiers
   */
  /*
    Check if a purchase can be made, check startTime, endTime
    and update Stage if so
  */
  modifier validPurchase {
    bool withinPeriod = now >= startTime && now <= endTime;
    require(msg.value > 0 && withinPeriod && startTime != 0);
    /*
      Update current stage only if the previous stage is `Waiting`
      and we are within the crowdsale period, used to automatically
      update the stage by an investor
    */
    if (withinPeriod == true && currentStage == Stage.Waiting) {
      currentStage = Stage.Running;
    }
    _;
  }
  // Allow only the owner of this contract
  modifier onlyOwner {
    require(msg.sender == owner);
    _;
  }

  // Allow only if is close to start time or if is it running
  modifier closeOrDuringCrowdsale {
    require(now >= startTime - intervalUpdate);
    require(currentStage == Stage.Running || currentStage == Stage.Waiting);
    _;
  }

  // Check if the provided stage is the current one
  modifier mustBeAtStage (Stage stageNeeded) {
    require(stageNeeded == currentStage);
    _;
  }

  /**
   * @dev Constructor
   * @param _wallet Where to send ETH collected from the ico
   */
  function Ico(address _wallet) public {
    require(phiRate > 0 && startTime == 0);
    require(_wallet != address(0));

    // update global variable
    wallet = _wallet;
    owner = msg.sender;

    currentStage = Stage.ToInitialize;

    // set Oraclize gas price (for __callback)
    oraclize_setCustomGasPrice(2500000000); // 2.5 gwei instead of 20 gwei
  }

  /**
   * @dev Used to init crowdsale, set start-end time, start usd rate update
   * @notice You must send some ETH to cover the oraclize_query fees
   * @param _startTime Start of the crowdsale (UNIX timestamp)
   * @param _endTime End of the crowdsale (UNIX timestamp)
   * @param _token Address of the PHI ERC223 Token
   */
  function initializeCrowdsale(
    uint256 _startTime,
    uint256 _endTime,
    address _token,
    uint256 _intervalUpdate
  )
    public
    payable
    onlyOwner
    mustBeAtStage(Stage.ToInitialize)
  {
    require(_startTime >= now);
    require(_endTime >= _startTime);
    require(_token != address(0));
    require(msg.value > 0);
    require(isContract(_token) == true);

    // interval update must be above or equal to 5 seconds
    require(_intervalUpdate >= 5);

    // update global variables
    startTime = _startTime;
    endTime = _endTime;
    token = Token(_token);
    intervalUpdate = _intervalUpdate;

    // update stage
    currentStage = Stage.Waiting;

    /*
      start to fetch ETH/USD price `intervalUpdate` before the `startTime`,
      30 seconds is added because Oraclize takes time to call the __callback
    */
    updateEthRateWithDelay(startTime - (intervalUpdate + 30));

    LogCrowdsaleInit();

    // check amount of tokens held by this contract matches MAX_TOKENS
    assert(token.balanceOf(address(this)) == MAX_TOKENS);
  }

  /* Oraclize related functions */
  /**
   * @dev Callback function used by Oraclize to update the price.
   * @notice ETH/USD rate is receivd and converted to wei, this
   * functions is used also to automatically update the stage status
   * @param myid Unique identifier for Oraclize queries
   * @param result Result of the requested query
   */
  function __callback(
    bytes32 myid,
    string result
  ) 
    public
    closeOrDuringCrowdsale
  {
    if (msg.sender != oraclize_cbAddress()) revert();
    // parse to int and multiply by 10**18 to allow math operations
    uint256 usdRate = parseInt(result, 18);
    // do not allow 0
    require(usdRate > 0);

    ethUsd = usdRate;

    LogRateUpdate(ethUsd, now);

    // check if time is over
    if (hasEnded() == true) {
      currentStage = Stage.Finished;
    } else {
      updateEthRate();
      lastOracleUpdate = now;
    }
  }

  /**
   * @dev Update ETH/USD rate manually in case Oraclize is not
   * calling by our contract
   * @notice An integer is required (e.g. 870, 910), this function
   * will also multiplicate by 10**18
   * @param _newEthUsd New ETH/USD rate integer
   */
  function updateEthUsdManually (uint _newEthUsd) public onlyOwner {
    require(_newEthUsd > 0);
    uint256 newRate = _newEthUsd.mul(10**18);
    require(newRate > 0);
    ethUsd = newRate;
  } 

  /**
   * @dev Used to recursively call the oraclize query 
   * @notice This function will not throw in case the
   * interval update is exceeded, in this way the latest
   * update made to the ETH/USD rate is kept 
   */
  function updateEthRate () internal {
    // prevent multiple updates
    if(intervalUpdate > (now - lastOracleUpdate)) {}
    else {
      updateEthRateWithDelay(intervalUpdate);
    }
  }

  /**
   * @dev Change interval update
   * @param newInterval New interval rate (in seconds)
   */
  function changeIntervalUpdate (uint newInterval) public onlyOwner {
    require(newInterval >= 5);
    intervalUpdate = newInterval;
  }

  /**
   * @dev Helper function around oraclize_query
   * @notice Call oraclize_query with a delay in seconds
   * @param delay Delay in seconds 
   */
  function updateEthRateWithDelay (uint delay) internal {
    // delay cannot be below 5 seconds (too fast)
    require(delay >= 5);
    if (oraclize_getPrice("URL", ORACLIZE_GAS_LIMIT) > this.balance) {
      // Notify that we need a top up 
      LogBalanceRequired();
    } else {
        // Get ETH/USD rate from kraken API
        oraclize_query(
          delay,
          "URL",
          "json(https://api.kraken.com/0/public/Ticker?pair=ETHUSD).result.XETHZUSD.c.0",
          ORACLIZE_GAS_LIMIT
        );
    }
  }

  /**
   * @dev Allow owner to force rate update
   * @param delay Delay in seconds of oraclize_query, can be set to 5 (minimum)
   */
  function forceOraclizeUpdate (uint256 delay) public onlyOwner {
    updateEthRateWithDelay(delay);
  }

  /**
   * @dev Change Oraclize gas limit (used in oraclize_query)
   * @notice To be used in case the default gas cost is too low
   * @param newGas New gas to use (in wei)
   */
  function changeOraclizeGas(uint newGas) public onlyOwner {
    require(newGas > 0 && newGas <= 4000000);
    ORACLIZE_GAS_LIMIT = newGas;
  }

  /**
   * @dev Change Oraclize gas price
   * @notice To be used in case the default gas price is too low
   * @param _gasPrice Gas price in wei
   */
  function changeOraclizeGasPrice(uint _gasPrice) public onlyOwner {
    require(_gasPrice >= 1000000000); // minimum 1 gwei
    oraclize_setCustomGasPrice(_gasPrice);
  }

  /**
   * @dev Top up balance
   * @notice This function must be used **only if 
   * this contract balance is too low for oraclize_query
   * to be executed**
   * @param verifyCode Used only to allow people that read
   * the notice (not accidental)
   */
  function topUpBalance (uint verifyCode) public payable mustBeAtStage(Stage.Running) {
    // value is required
    require(msg.value > 0);
    // make sure only the people that read
    // this can use the function
    require(verifyCode == 28391728448);
  }

  /**
   * @dev Withdraw balance of this contract to the `wallet` address
   * @notice Used only if there are some leftover
   * funds (because of topUpBalance)
  */
  function withdrawBalance() public mustBeAtStage(Stage.Finalized) {
    wallet.transfer(this.balance);
  }

  /* Invest related functions */
  /**
   * @dev Fallback function, to be used to purchase tokens
   */
  function () public payable validPurchase mustBeAtStage(Stage.Running) {
    require(msg.sender != address(0));
    // cannot be a contract
    require(isContract(msg.sender) == false);

    address beneficiary = msg.sender;

    uint256 weiAmount = msg.value;

    // calculate token amount to be transfered
    uint256 tokens = getTokenAmount(weiAmount);

    require(tokens > 0);
    // check if we are below MAX_TOKENS
    require(tokensSold.add(tokens) <= MAX_TOKENS);

    // update tokens sold
    tokensSold = tokensSold.add(tokens);

    // update total wei counter
    weiRaised = weiRaised.add(weiAmount);

    // update balance of the beneficiary
    balancesToken[beneficiary] = balancesToken[beneficiary].add(tokens);
    balancesWei[beneficiary] = balancesWei[beneficiary].add(weiAmount);

    TokenPurchase(msg.sender, beneficiary, weiAmount, tokens);

    // forward ETH
    forwardFunds();
  }

  /**
   * @dev send ether to the fund collection wallet
   */
  function forwardFunds() internal {
    wallet.transfer(msg.value);
  }

  /* Finalize/whitelist functions */
  /**
   * @dev Add multiple whitelisted addresses
   * @notice Must be called after the crowdsale has finished
   * @param investors Array or investors to enable
   */ 
  function addWhitelistAddrByList (address[] investors) public onlyOwner mustBeAtStage(Stage.Finished) {
    for(uint256 i = 0; i < investors.length; i++) {
      addWhitelistAddress(investors[i]);
    }
  }

  /**
   * @dev Whitelist a specific address
   * @notice This is mainly an helper function but can be useful in
   * case the `addWhitelistAddrs` loop has issues
   * @param investor Investor to whitelist
   */ 
  function addWhitelistAddress (address investor) public onlyOwner mustBeAtStage(Stage.Finished) {
    require(investor != address(0) && investor != address(this));
    require(isWhitelisted[investor] == false);
    require(balancesToken[investor] > 0);
    isWhitelisted[investor] = true;
  }

  /**
   * @dev Remove an address from whitelist
   */
  function removeWhitelistedAddress (address toRemove) public onlyOwner mustBeAtStage(Stage.Finished) {
    require(isWhitelisted[toRemove] == true);
    isWhitelisted[toRemove] = false;
  }

  /**
   * @dev Finalize crowdsale with investors array
   * @notice Transfers tokens to whitelisted addresses
   */
  function finalizeInvestorsByList(address[] investors) public onlyOwner mustBeAtStage(Stage.Finished) {
    for(uint256 i = 0; i < investors.length; i++) {
      finalizeSingleInvestor(investors[i]);
    }
  }

  /**
   * @dev Finalize a specific investor
   * @notice This is mainly an helper function to `finalize` but
   * can be used if `finalize` has issues with the loop
   * @param investorAddr Address to finalize
   */
  function finalizeSingleInvestor(address investorAddr) public onlyOwner mustBeAtStage(Stage.Finished) {
    require(investorAddr != address(0) && investorAddr != address(this));
    require(balancesToken[investorAddr] > 0);
    require(isWhitelisted[investorAddr] == true);
    // save data into variables
    uint256 balanceToTransfer = balancesToken[investorAddr];
    // reset current state
    balancesToken[investorAddr] = 0;
    isWhitelisted[investorAddr] = false;
    // transfer token to the investor address and the balance
    // that we have recorded before
    require(token.transfer(investorAddr, balanceToTransfer));
    // update tokens sent
    tokensFinalized = tokensFinalized.add(balanceToTransfer);
    assert(tokensFinalized <= MAX_TOKENS);
  }

  /**
   * @dev Burn unsold tokens
   * @notice Call this function after finalizing
   */
  function burnRemainingTokens() public onlyOwner mustBeAtStage(Stage.Finalized) {
    // should always be true
    require(MAX_TOKENS >= tokensFinalized);
    uint unsold = MAX_TOKENS.sub(tokensFinalized);
    if (unsold > 0) {
      token.burn(unsold);
    }
  }

  /**
   * @dev Burn all remaining tokens held by this contract
   * @notice Get the token balance of this contract and burns all tokens
   */
  function burnAllTokens() public onlyOwner mustBeAtStage(Stage.Finalized) {
    uint thisTokenBalance = token.balanceOf(address(this));
    if (thisTokenBalance > 0) {
      token.burn(thisTokenBalance);
    }
  }

  /**
   * @dev Allow to change the current stage
   * @param newStage New stage
   */
  function changeStage (Stage newStage) public onlyOwner {
    currentStage = newStage;
  }

  /**
   * @dev ico status (based only on time)
   * @return true if crowdsale event has ended
   */
  function hasEnded() public constant returns (bool) {
    return now > endTime && startTime != 0;
  }

  /* Price functions */
  /**
   * @dev Get current ETH/PHI rate (1 ETH = getEthPhiRate() PHI)
   * @notice It divides (ETH/USD rate) / (PHI/USD rate), use the
   * custom function `getEthPhiRate(false)` if you want a more
   * accurate rate
   * @return ETH/PHI rate
   */
  function getEthPhiRate () public constant returns (uint) {
    // 1/(ETH/PHI rate) * (ETH/USD rate) should return PHI rate
    // multiply by 1000 to keep decimals from the division
    return ethUsd.div(phiRate);
  }
  /**
   * @dev Get current kETH/PHI rate (1000 ETH = getkEthPhiRate() PHI)
   * used to get a more accurate rate (by not truncating decimals)
   * @notice It divides (ETH/USD rate) / (PHI/USD rate)
   * @return kETH/PHI rate
   */
  function getkEthPhiRate () public constant returns (uint) {
    // 1/(kETH/PHI rate) * (ETH/USD rate) should return PHI rate
    // multiply by 1000 to keep decimals from the division, and return kEth/PHI rate
    return ethUsd.mul(1000).div(phiRate);
  }

  /**
   * @dev Calculate amount of token based on wei amount
   * @param weiAmount Amount of wei
   * @return Amount of PHI tokens
   */
  function getTokenAmount(uint256 weiAmount) public constant returns(uint256) {
    // get kEth rate to keep decimals
    uint currentKethRate = getkEthPhiRate();
    // divide by 1000 to revert back the multiply
    return currentKethRate.mul(weiAmount).div(1000);
  }

  /* Helper functions for token balance */
  /**
   * @dev Returns how many tokens an investor has
   * @param investor Investor to look for
   * @return Balance of the investor
   */
  function getInvestorBalance(address investor) external constant returns (uint) {
    return balancesToken[investor];
  }

  /**
   * @dev Returns how many wei an investor has invested
   * @param investor Investor to look for
   * @return Balance of the investor
   */
  function getInvestorWeiBalance(address investor) external constant returns (uint) {
    return balancesWei[investor];
  }

  /**
   * @dev Check if an address is a contract
   * @param addr Address to check
   * @return True if is a contract
   */
  function isContract(address addr) public constant returns (bool) {
    uint size;
    assembly { size := extcodesize(addr) }
    return size > 0;
  }

  /* Ico engine compatible functions */
  /**
   * @dev Return `started` state
   * false if the crowdsale is not started,
   * true if the crowdsale is started and running,
   * true if the crowdsale is completed
   * @return crowdsale state
   */
  function started() public view returns(bool) {
    if ((uint8(currentStage) >= 2 || now >= startTime && now <= endTime) && uint8(currentStage) != 3) return true;
    return false;
  }

  /**
   * @dev Return if crowdsale ended
   * false if the crowdsale is not started,
   * false if the crowdsale is started and running,
   * true if the crowdsale is completed
   * @return ended state
   */
  function ended() public view returns(bool) {
    if (tokensSold == MAX_TOKENS) return true;
    if (uint8(currentStage) >= 4) return true;
    return hasEnded();
  }

  /**
   * @dev returns the total number of the tokens available for the sale,
   * must not change when the crowdsale is started
   * @return total tokens in sphi
   */
  function totalTokens() public view returns(uint) {
    return MAX_TOKENS;
  }
  
  /**
   * @dev returns the number of the tokens available for the crowdsale.
   * At the moment that the crowdsale starts it must be equal to totalTokens(),
   * then it will decrease. It is used to calculate the
   * percentage of sold tokens as remainingTokens() / totalTokens()
   * @return Remaining tokens in sphi
   */
  function remainingTokens() public view returns(uint) {
    return MAX_TOKENS.sub(tokensSold);
  }

  /**
   * @dev return the price as number of tokens released for each ether
   * @return amount in sphi for 1 ether
   */
  function price() public view returns(uint) {
    return getTokenAmount(1 ether);
  }
}
