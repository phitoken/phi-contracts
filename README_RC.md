### Reservation contract

Used to "proxy" investments to pre-ico (permission investment)

To note:

 * Investment limit (1 eth)
 * Allow only addresses (no external contracts)

#### Requirments

 * Node version 7.10 or above

 * Debian

#### Install

cd into the token folder and run `npm install`

Run the ethereum test client provided (**only if you want to deploy/test contracts**):

`npm run testrpc`

Compile contracts and run migrations with truffle:

`npm run setup`

This will compile smart contracts, generates ABI and deploy it on the default network.

#### Tests

Reservation contract is tested through pre-ico tests

#### Docs

Docs for RC is available [here](docs/RC.md)

You can update it by running `npm run docs:build`

** Please note ** `solc` is required in your environment

