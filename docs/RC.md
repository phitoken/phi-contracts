* [ReservationContract](#reservationcontract)
  * [isContract](#function-iscontract)
  * [preIcoStart](#function-preicostart)
  * [MIN_INVESTMENT](#function-min_investment)
  * [invested](#function-invested)
  * [preIcoAddr](#function-preicoaddr)
  * [preIcoEnd](#function-preicoend)

# ReservationContract


## *function* isContract

ReservationContract.isContract(addr) `view` `16279055`

> Check if an address is a contract

Inputs

| | | |
|-|-|-|
| *address* | addr | Address to check |

Outputs

| | | |
|-|-|-|
| *bool* |  | undefined |

## *function* preIcoStart

ReservationContract.preIcoStart() `view` `3d741994`





## *function* MIN_INVESTMENT

ReservationContract.MIN_INVESTMENT() `view` `4ef8ff33`





## *function* invested

ReservationContract.invested() `view` `66b3f6bf`


Inputs

| | | |
|-|-|-|
| *address* |  | undefined |


## *function* preIcoAddr

ReservationContract.preIcoAddr() `view` `7fa34e92`





## *function* preIcoEnd

ReservationContract.preIcoEnd() `view` `833270d8`







---