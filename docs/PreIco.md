* [PreIco](#preico)
  * [addNewReservContract](#function-addnewreservcontract)
  * [ended](#function-ended)
  * [tokensFinalized](#function-tokensfinalized)
  * [isContract](#function-iscontract)
  * [started](#function-started)
  * [__callback](#function-__callback)
  * [endTime](#function-endtime)
  * [finalizeInvestorsByList](#function-finalizeinvestorsbylist)
  * [__callback](#function-__callback)
  * [isWhitelisted](#function-iswhitelisted)
  * [weiRaised](#function-weiraised)
  * [ORACLIZE_GAS_LIMIT](#function-oraclize_gas_limit)
  * [tokensSold](#function-tokenssold)
  * [wallet](#function-wallet)
  * [addWhitelistAddrByList](#function-addwhitelistaddrbylist)
  * [removeWhitelistedAddress](#function-removewhitelistedaddress)
  * [getInvestorWeiBalance](#function-getinvestorweibalance)
  * [ethUsd](#function-ethusd)
  * [currentStage](#function-currentstage)
  * [getInvestorBalance](#function-getinvestorbalance)
  * [changeOraclizeGas](#function-changeoraclizegas)
  * [withdrawBalance](#function-withdrawbalance)
  * [getkEthPhiRate](#function-getkethphirate)
  * [finalizeSingleInvestor](#function-finalizesingleinvestor)
  * [startTime](#function-starttime)
  * [reservationContracts](#function-reservationcontracts)
  * [totalTokens](#function-totaltokens)
  * [burnRemainingTokens](#function-burnremainingtokens)
  * [updateEthUsdManually](#function-updateethusdmanually)
  * [initializeCrowdsale](#function-initializecrowdsale)
  * [balancesWei](#function-balanceswei)
  * [owner](#function-owner)
  * [addWhitelistAddress](#function-addwhitelistaddress)
  * [price](#function-price)
  * [changeIntervalUpdate](#function-changeintervalupdate)
  * [topUpBalance](#function-topupbalance)
  * [getEthPhiRate](#function-getethphirate)
  * [lastOracleUpdate](#function-lastoracleupdate)
  * [changeStage](#function-changestage)
  * [remainingTokens](#function-remainingtokens)
  * [changeOraclizeGasPrice](#function-changeoraclizegasprice)
  * [burnAllTokens](#function-burnalltokens)
  * [getTokenAmount](#function-gettokenamount)
  * [phiRate](#function-phirate)
  * [removeReservContract](#function-removereservcontract)
  * [intervalUpdate](#function-intervalupdate)
  * [forceOraclizeUpdate](#function-forceoraclizeupdate)
  * [buyTokens](#function-buytokens)
  * [hasEnded](#function-hasended)
  * [MAX_TOKENS](#function-max_tokens)
  * [token](#function-token)
  * [balancesToken](#function-balancestoken)
  * [TokenPurchase](#event-tokenpurchase)
  * [LogRateUpdate](#event-lograteupdate)
  * [LogBalanceRequired](#event-logbalancerequired)
  * [LogCrowdsaleInit](#event-logcrowdsaleinit)
* [SafeMath](#safemath)
* [Token](#token)
  * [approve](#function-approve)
  * [transferFrom](#function-transferfrom)
  * [burn](#function-burn)
  * [balanceOf](#function-balanceof)
  * [transfer](#function-transfer)
  * [transfer](#function-transfer)
  * [transferDuringLockTime](#function-transferduringlocktime)
  * [allowance](#function-allowance)
  * [Transfer](#event-transfer)
  * [Approval](#event-approval)
  * [Burn](#event-burn)
* [usingOraclize](#usingoraclize)
  * [__callback](#function-__callback)
  * [__callback](#function-__callback)

# PreIco


## *function* addNewReservContract

PreIco.addNewReservContract(newRcAddr) `nonpayable` `002854de`

**Call this before the startTime to avoid delays**

> Allow a new reservation contract to invest in the pre-ico

Inputs

| | | |
|-|-|-|
| *address* | newRcAddr | Address of the reservation contract you want to enable (must be a contract) |


## *function* ended

PreIco.ended() `view` `12fa6feb`

> Return if crowdsale ended false if the crowdsale is not started, false if the crowdsale is started and running, true if the crowdsale is completed



Outputs

| | | |
|-|-|-|
| *bool* |  | undefined |

## *function* tokensFinalized

PreIco.tokensFinalized() `view` `13040bc1`





## *function* isContract

PreIco.isContract(addr) `view` `16279055`

> Check if an address is a contract

Inputs

| | | |
|-|-|-|
| *address* | addr | Address to check |

Outputs

| | | |
|-|-|-|
| *bool* |  | undefined |

## *function* started

PreIco.started() `view` `1f2698ab`

> Return `started` state false if the crowdsale is not started, true if the crowdsale is started and running, true if the crowdsale is completed



Outputs

| | | |
|-|-|-|
| *bool* |  | undefined |

## *function* __callback

PreIco.__callback(myid, result) `nonpayable` `27dc297e`

**ETH/USD rate is receivd and converted to wei, this functions is used also to automatically update the stage status**

> Callback function used by Oraclize to update the price.

Inputs

| | | |
|-|-|-|
| *bytes32* | myid | Unique identifier for Oraclize queries |
| *string* | result | Result of the requested query |


## *function* endTime

PreIco.endTime() `view` `3197cbb6`





## *function* finalizeInvestorsByList

PreIco.finalizeInvestorsByList(investors) `nonpayable` `364595a7`

**Transfers tokens to whitelisted addresses**

> Finalize crowdsale with investors array

Inputs

| | | |
|-|-|-|
| *address[]* | investors | undefined |


## *function* __callback

PreIco.__callback(myid, result, proof) `nonpayable` `38bbfa50`


Inputs

| | | |
|-|-|-|
| *bytes32* | myid | undefined |
| *string* | result | undefined |
| *bytes* | proof | undefined |


## *function* isWhitelisted

PreIco.isWhitelisted() `view` `3af32abf`


Inputs

| | | |
|-|-|-|
| *address* |  | undefined |


## *function* weiRaised

PreIco.weiRaised() `view` `4042b66f`





## *function* ORACLIZE_GAS_LIMIT

PreIco.ORACLIZE_GAS_LIMIT() `view` `4eb8ffb4`





## *function* tokensSold

PreIco.tokensSold() `view` `518ab2a8`





## *function* wallet

PreIco.wallet() `view` `521eb273`





## *function* addWhitelistAddrByList

PreIco.addWhitelistAddrByList(investors) `nonpayable` `5262b0a0`

**Must be called after the crowdsale has finished**

> Add multiple whitelisted addresses

Inputs

| | | |
|-|-|-|
| *address[]* | investors | Array or investors to enable |


## *function* removeWhitelistedAddress

PreIco.removeWhitelistedAddress(toRemove) `nonpayable` `530cd5ab`

> Remove an address from whitelist

Inputs

| | | |
|-|-|-|
| *address* | toRemove | undefined |


## *function* getInvestorWeiBalance

PreIco.getInvestorWeiBalance(investor) `view` `58c1c355`

> Returns how many wei an investor has invested

Inputs

| | | |
|-|-|-|
| *address* | investor | Investor to look for |

Outputs

| | | |
|-|-|-|
| *uint256* |  | undefined |

## *function* ethUsd

PreIco.ethUsd() `view` `5a960216`





## *function* currentStage

PreIco.currentStage() `view` `5bf5d54c`





## *function* getInvestorBalance

PreIco.getInvestorBalance(investor) `view` `5ea63913`

> Returns how many tokens an investor has

Inputs

| | | |
|-|-|-|
| *address* | investor | Investor to look for |

Outputs

| | | |
|-|-|-|
| *uint256* |  | undefined |

## *function* changeOraclizeGas

PreIco.changeOraclizeGas(newGas) `nonpayable` `5eb572ad`

**To be used in case the default gas cost is too low**

> Change Oraclize gas limit (used in oraclize_query)

Inputs

| | | |
|-|-|-|
| *uint256* | newGas | New gas to use (in wei) |


## *function* withdrawBalance

PreIco.withdrawBalance() `nonpayable` `5fd8c710`

**Used only if there are some leftover funds (because of topUpBalance)**

> Withdraw balance of this contract to the `wallet` address




## *function* getkEthPhiRate

PreIco.getkEthPhiRate() `view` `6a470988`

**It divides (ETH/USD rate) / (PHI/USD rate)**

> Get current kETH/PHI rate (1000 ETH = getkEthPhiRate() PHI) used to get a more accurate rate (by not truncating decimals)



Outputs

| | | |
|-|-|-|
| *uint256* |  | undefined |

## *function* finalizeSingleInvestor

PreIco.finalizeSingleInvestor(investorAddr) `nonpayable` `72efba45`

**This is mainly an helper function to `finalize` but can be used if `finalize` has issues with the loop**

> Finalize a specific investor

Inputs

| | | |
|-|-|-|
| *address* | investorAddr | Address to finalize |


## *function* startTime

PreIco.startTime() `view` `78e97925`





## *function* reservationContracts

PreIco.reservationContracts() `view` `7904bd09`


Inputs

| | | |
|-|-|-|
| *address* |  | undefined |


## *function* totalTokens

PreIco.totalTokens() `view` `7e1c0c09`

> returns the total number of the tokens available for the sale, must not change when the crowdsale is started



Outputs

| | | |
|-|-|-|
| *uint256* |  | undefined |

## *function* burnRemainingTokens

PreIco.burnRemainingTokens() `nonpayable` `83408d73`

**Call this function after finalizing**

> Burn unsold tokens




## *function* updateEthUsdManually

PreIco.updateEthUsdManually(_newEthUsd) `nonpayable` `8a99cdc7`

**An integer is required (e.g. 870, 910), this function will also multiplicate by 10**18**

> Update ETH/USD rate manually in case Oraclize is not calling by our contract

Inputs

| | | |
|-|-|-|
| *uint256* | _newEthUsd | New ETH/USD rate integer |


## *function* initializeCrowdsale

PreIco.initializeCrowdsale(_startTime, _endTime, _token, _intervalUpdate) `payable` `8b5f37ee`

**You must send some ETH to cover the oraclize_query fees**

> Used to init crowdsale, set start-end time, start usd rate update

Inputs

| | | |
|-|-|-|
| *uint256* | _startTime | Start of the crowdsale (UNIX timestamp) |
| *uint256* | _endTime | End of the crowdsale (UNIX timestamp) |
| *address* | _token | Address of the PHI ERC223 Token |
| *uint256* | _intervalUpdate | undefined |


## *function* balancesWei

PreIco.balancesWei() `view` `8c328dbf`


Inputs

| | | |
|-|-|-|
| *address* |  | undefined |


## *function* owner

PreIco.owner() `view` `8da5cb5b`





## *function* addWhitelistAddress

PreIco.addWhitelistAddress(investor) `nonpayable` `94a7ef15`

**This is mainly an helper function but can be useful in case the `addWhitelistAddrs` loop has issues**

> Whitelist a specific address

Inputs

| | | |
|-|-|-|
| *address* | investor | Investor to whitelist |


## *function* price

PreIco.price() `view` `a035b1fe`

> return the price as number of tokens released for each ether



Outputs

| | | |
|-|-|-|
| *uint256* |  | undefined |

## *function* changeIntervalUpdate

PreIco.changeIntervalUpdate(newInterval) `nonpayable` `a1d50cb9`

> Change interval update

Inputs

| | | |
|-|-|-|
| *uint256* | newInterval | New interval rate (in seconds) |


## *function* topUpBalance

PreIco.topUpBalance(verifyCode) `payable` `a635392d`

**This function must be used **only if  this contract balance is too low for oraclize_query to be executed****

> Top up balance

Inputs

| | | |
|-|-|-|
| *uint256* | verifyCode | Used only to allow people that read the notice (not accidental) |


## *function* getEthPhiRate

PreIco.getEthPhiRate() `view` `b14debe4`

**It divides (ETH/USD rate) / (PHI/USD rate), use the custom function `getEthPhiRate(false)` if you want a more accurate rate**

> Get current ETH/PHI rate (1 ETH = getEthPhiRate() PHI)



Outputs

| | | |
|-|-|-|
| *uint256* |  | undefined |

## *function* lastOracleUpdate

PreIco.lastOracleUpdate() `view` `b609d0e6`





## *function* changeStage

PreIco.changeStage(newStage) `nonpayable` `b9172dec`

> Allow to change the current stage

Inputs

| | | |
|-|-|-|
| *uint8* | newStage | New stage |


## *function* remainingTokens

PreIco.remainingTokens() `view` `bf583903`

> returns the number of the tokens available for the crowdsale. At the moment that the crowdsale starts it must be equal to totalTokens(), then it will decrease. It is used to calculate the percentage of sold tokens as remainingTokens() / totalTokens()



Outputs

| | | |
|-|-|-|
| *uint256* |  | undefined |

## *function* changeOraclizeGasPrice

PreIco.changeOraclizeGasPrice(_gasPrice) `nonpayable` `c0c1b107`

**To be used in case the default gas price is too low**

> Change Oraclize gas price

Inputs

| | | |
|-|-|-|
| *uint256* | _gasPrice | Gas price in wei |


## *function* burnAllTokens

PreIco.burnAllTokens() `nonpayable` `c17e2aa1`

**Get the token balance of this contract and burns all tokens**

> Burn all remaining tokens held by this contract




## *function* getTokenAmount

PreIco.getTokenAmount(weiAmount) `view` `c2507ac1`

> Calculate amount of token based on wei amount

Inputs

| | | |
|-|-|-|
| *uint256* | weiAmount | Amount of wei |

Outputs

| | | |
|-|-|-|
| *uint256* |  | undefined |

## *function* phiRate

PreIco.phiRate() `view` `c30d0ee4`





## *function* removeReservContract

PreIco.removeReservContract(toRemove) `nonpayable` `d3db4cb0`

> Remove a reservation contract

Inputs

| | | |
|-|-|-|
| *address* | toRemove | Address of the reservation contract you want to remove |


## *function* intervalUpdate

PreIco.intervalUpdate() `view` `e067f631`





## *function* forceOraclizeUpdate

PreIco.forceOraclizeUpdate(delay) `nonpayable` `e2cf55e6`

> Allow owner to force rate update

Inputs

| | | |
|-|-|-|
| *uint256* | delay | Delay in seconds of oraclize_query, can be set to 5 (minimum) |


## *function* buyTokens

PreIco.buyTokens(beneficiary) `payable` `ec8ac4d8`

**If you call directly this function your are buying for someone else**

> Low level function to purchase function on behalf of a beneficiary

Inputs

| | | |
|-|-|-|
| *address* | beneficiary | Where tokens should be sent |


## *function* hasEnded

PreIco.hasEnded() `view` `ecb70fb7`

> pre-ico status (based only on time)



Outputs

| | | |
|-|-|-|
| *bool* |  | undefined |

## *function* MAX_TOKENS

PreIco.MAX_TOKENS() `view` `f47c84c5`





## *function* token

PreIco.token() `view` `fc0c546a`





## *function* balancesToken

PreIco.balancesToken() `view` `fc996038`


Inputs

| | | |
|-|-|-|
| *address* |  | undefined |


## *event* TokenPurchase

PreIco.TokenPurchase(purchaser, beneficiary, value, amount) `623b3804`

Arguments

| | | |
|-|-|-|
| *address* | purchaser | indexed |
| *address* | beneficiary | indexed |
| *uint256* | value | not indexed |
| *uint256* | amount | not indexed |

## *event* LogRateUpdate

PreIco.LogRateUpdate(newRate, timeRecv) `d8146a2e`

Arguments

| | | |
|-|-|-|
| *uint256* | newRate | not indexed |
| *uint256* | timeRecv | not indexed |

## *event* LogBalanceRequired

PreIco.LogBalanceRequired() `f2eff06c`



## *event* LogCrowdsaleInit

PreIco.LogCrowdsaleInit() `9e953e39`




---
# SafeMath


---
# Token


## *function* approve

Token.approve(_spender, _value) `nonpayable` `095ea7b3`


Inputs

| | | |
|-|-|-|
| *address* | _spender | undefined |
| *uint256* | _value | undefined |


## *function* transferFrom

Token.transferFrom(_from, _to, _value) `nonpayable` `23b872dd`


Inputs

| | | |
|-|-|-|
| *address* | _from | undefined |
| *address* | _to | undefined |
| *uint256* | _value | undefined |


## *function* burn

Token.burn(num) `nonpayable` `42966c68`


Inputs

| | | |
|-|-|-|
| *uint256* | num | undefined |


## *function* balanceOf

Token.balanceOf(_owner) `view` `70a08231`


Inputs

| | | |
|-|-|-|
| *address* | _owner | undefined |


## *function* transfer

Token.transfer(_to, _value) `nonpayable` `a9059cbb`


Inputs

| | | |
|-|-|-|
| *address* | _to | undefined |
| *uint256* | _value | undefined |


## *function* transfer

Token.transfer(_to, _value, _data) `nonpayable` `be45fd62`


Inputs

| | | |
|-|-|-|
| *address* | _to | undefined |
| *uint256* | _value | undefined |
| *bytes* | _data | undefined |


## *function* transferDuringLockTime

Token.transferDuringLockTime(_from, _to, _value) `nonpayable` `dca4740b`


Inputs

| | | |
|-|-|-|
| *address* | _from | undefined |
| *address* | _to | undefined |
| *uint256* | _value | undefined |


## *function* allowance

Token.allowance(_owner, _spender) `view` `dd62ed3e`


Inputs

| | | |
|-|-|-|
| *address* | _owner | undefined |
| *address* | _spender | undefined |

## *event* Transfer

Token.Transfer(_from, _to, _value) `ddf252ad`

Arguments

| | | |
|-|-|-|
| *address* | _from | indexed |
| *address* | _to | indexed |
| *uint256* | _value | not indexed |

## *event* Approval

Token.Approval(_owner, _spender, _value) `8c5be1e5`

Arguments

| | | |
|-|-|-|
| *address* | _owner | indexed |
| *address* | _spender | indexed |
| *uint256* | _value | not indexed |

## *event* Burn

Token.Burn(_burner, _value) `cc16f5db`

Arguments

| | | |
|-|-|-|
| *address* | _burner | indexed |
| *uint256* | _value | not indexed |


---
# usingOraclize


## *function* __callback

usingOraclize.__callback(myid, result) `nonpayable` `27dc297e`


Inputs

| | | |
|-|-|-|
| *bytes32* | myid | undefined |
| *string* | result | undefined |


## *function* __callback

usingOraclize.__callback(myid, result, proof) `nonpayable` `38bbfa50`


Inputs

| | | |
|-|-|-|
| *bytes32* | myid | undefined |
| *string* | result | undefined |
| *bytes* | proof | undefined |


---