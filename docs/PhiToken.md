* [ERC223ReceivingContract](#erc223receivingcontract)
  * [tokenFallback](#function-tokenfallback)
* [PhiToken](#phitoken)
  * [name](#function-name)
  * [approve](#function-approve)
  * [lockTime](#function-locktime)
  * [totalSupply](#function-totalsupply)
  * [transferFrom](#function-transferfrom)
  * [decimals](#function-decimals)
  * [burn](#function-burn)
  * [WALLET_ADDR](#function-wallet_addr)
  * [ICO_ADDR](#function-ico_addr)
  * [balanceOf](#function-balanceof)
  * [PRE_ICO_ADDR](#function-pre_ico_addr)
  * [symbol](#function-symbol)
  * [transfer](#function-transfer)
  * [transfer](#function-transfer)
  * [transferDuringLockTime](#function-transferduringlocktime)
  * [allowance](#function-allowance)
  * [Deployed](#event-deployed)
  * [Transfer](#event-transfer)
  * [Approval](#event-approval)
  * [Burn](#event-burn)
* [SafeMath](#safemath)
* [Token](#token)
  * [approve](#function-approve)
  * [transferFrom](#function-transferfrom)
  * [burn](#function-burn)
  * [balanceOf](#function-balanceof)
  * [transfer](#function-transfer)
  * [transfer](#function-transfer)
  * [transferDuringLockTime](#function-transferduringlocktime)
  * [allowance](#function-allowance)
  * [Transfer](#event-transfer)
  * [Approval](#event-approval)
  * [Burn](#event-burn)

# ERC223ReceivingContract


## *function* tokenFallback

ERC223ReceivingContract.tokenFallback(_from, _value, _data) `nonpayable` `c0ee0b8a`

> Function that is called when a user or another contract wants to transfer funds.

Inputs

| | | |
|-|-|-|
| *address* | _from | Transaction initiator, analogue of msg.sender |
| *uint256* | _value | Number of tokens to transfer. |
| *bytes* | _data | Data containig a function signature and/or parameters |


---
# PhiToken


## *function* name

PhiToken.name() `view` `06fdde03`





## *function* approve

PhiToken.approve(_spender, _value) `nonpayable` `095ea7b3`

**Allows `_spender` to transfer `_value` tokens from `msg.sender` to any address.**

> Sets approved amount of tokens for spender. Returns success.

Inputs

| | | |
|-|-|-|
| *address* | _spender | Address of allowed account. |
| *uint256* | _value | Number of approved tokens. |

Outputs

| | | |
|-|-|-|
| *bool* |  | undefined |

## *function* lockTime

PhiToken.lockTime() `view` `0d668087`





## *function* totalSupply

PhiToken.totalSupply() `view` `18160ddd`





## *function* transferFrom

PhiToken.transferFrom(_from, _to, _value) `nonpayable` `23b872dd`

**Transfer `_value` tokens from `_from` to `_to` if `msg.sender` is allowed.**

> Allows for an approved third party to transfer tokens from one address to another. Returns success.

Inputs

| | | |
|-|-|-|
| *address* | _from | Address from where tokens are withdrawn. |
| *address* | _to | Address to where tokens are sent. |
| *uint256* | _value | Number of tokens to transfer. |

Outputs

| | | |
|-|-|-|
| *bool* |  | undefined |

## *function* decimals

PhiToken.decimals() `view` `313ce567`





## *function* burn

PhiToken.burn(_value) `nonpayable` `42966c68`

**Allows `msg.sender` to simply destroy `_value` token units (sphi). This means the total token supply will decrease.**

> Allows to destroy token units (sphi).

Inputs

| | | |
|-|-|-|
| *uint256* | _value | Number of token units (sphi) to burn. |


## *function* WALLET_ADDR

PhiToken.WALLET_ADDR() `view` `44169752`





## *function* ICO_ADDR

PhiToken.ICO_ADDR() `view` `54e0b464`





## *function* balanceOf

PhiToken.balanceOf(_owner) `view` `70a08231`

> Returns number of tokens owned by the given address.

Inputs

| | | |
|-|-|-|
| *address* | _owner | Address of token owner. |

Outputs

| | | |
|-|-|-|
| *uint256* |  | undefined |

## *function* PRE_ICO_ADDR

PhiToken.PRE_ICO_ADDR() `view` `799b7bb8`





## *function* symbol

PhiToken.symbol() `view` `95d89b41`





## *function* transfer

PhiToken.transfer(_to, _value) `nonpayable` `a9059cbb`

**Send `_value` tokens to `_to` from `msg.sender`.**

> Transfers sender's tokens to a given address. Returns success.

Inputs

| | | |
|-|-|-|
| *address* | _to | Address of token receiver. |
| *uint256* | _value | Number of tokens to transfer. |

Outputs

| | | |
|-|-|-|
| *bool* |  | undefined |

## *function* transfer

PhiToken.transfer(_to, _value, _data) `nonpayable` `be45fd62`

**Send `_value` tokens to `_to` from `msg.sender` and trigger tokenFallback if sender is a contract.**

> Function that is called when a user or another contract wants to transfer funds.

Inputs

| | | |
|-|-|-|
| *address* | _to | Address of token receiver. |
| *uint256* | _value | Number of tokens to transfer. |
| *bytes* | _data | Data to be sent to tokenFallback |

Outputs

| | | |
|-|-|-|
| *bool* |  | undefined |

## *function* transferDuringLockTime

PhiToken.transferDuringLockTime(_from, _to, _value) `nonpayable` `dca4740b`


Inputs

| | | |
|-|-|-|
| *address* | _from | undefined |
| *address* | _to | undefined |
| *uint256* | _value | undefined |


## *function* allowance

PhiToken.allowance(_owner, _spender) `view` `dd62ed3e`

> Returns number of allowed tokens that a spender can transfer on behalf of a token owner.

Inputs

| | | |
|-|-|-|
| *address* | _owner | Address of token owner. |
| *address* | _spender | Address of token spender. |

Outputs

| | | |
|-|-|-|
| *uint256* |  | undefined |

## *event* Deployed

PhiToken.Deployed(_total_supply) `b94ae47e`

Arguments

| | | |
|-|-|-|
| *uint256* | _total_supply | indexed |

## *event* Transfer

PhiToken.Transfer(_from, _to, _value) `ddf252ad`

Arguments

| | | |
|-|-|-|
| *address* | _from | indexed |
| *address* | _to | indexed |
| *uint256* | _value | not indexed |

## *event* Approval

PhiToken.Approval(_owner, _spender, _value) `8c5be1e5`

Arguments

| | | |
|-|-|-|
| *address* | _owner | indexed |
| *address* | _spender | indexed |
| *uint256* | _value | not indexed |

## *event* Burn

PhiToken.Burn(_burner, _value) `cc16f5db`

Arguments

| | | |
|-|-|-|
| *address* | _burner | indexed |
| *uint256* | _value | not indexed |


---
# SafeMath


---
# Token


## *function* approve

Token.approve(_spender, _value) `nonpayable` `095ea7b3`


Inputs

| | | |
|-|-|-|
| *address* | _spender | undefined |
| *uint256* | _value | undefined |


## *function* transferFrom

Token.transferFrom(_from, _to, _value) `nonpayable` `23b872dd`


Inputs

| | | |
|-|-|-|
| *address* | _from | undefined |
| *address* | _to | undefined |
| *uint256* | _value | undefined |


## *function* burn

Token.burn(num) `nonpayable` `42966c68`


Inputs

| | | |
|-|-|-|
| *uint256* | num | undefined |


## *function* balanceOf

Token.balanceOf(_owner) `view` `70a08231`


Inputs

| | | |
|-|-|-|
| *address* | _owner | undefined |


## *function* transfer

Token.transfer(_to, _value) `nonpayable` `a9059cbb`


Inputs

| | | |
|-|-|-|
| *address* | _to | undefined |
| *uint256* | _value | undefined |


## *function* transfer

Token.transfer(_to, _value, _data) `nonpayable` `be45fd62`


Inputs

| | | |
|-|-|-|
| *address* | _to | undefined |
| *uint256* | _value | undefined |
| *bytes* | _data | undefined |


## *function* transferDuringLockTime

Token.transferDuringLockTime(_from, _to, _value) `nonpayable` `dca4740b`


Inputs

| | | |
|-|-|-|
| *address* | _from | undefined |
| *address* | _to | undefined |
| *uint256* | _value | undefined |


## *function* allowance

Token.allowance(_owner, _spender) `view` `dd62ed3e`


Inputs

| | | |
|-|-|-|
| *address* | _owner | undefined |
| *address* | _spender | undefined |

## *event* Transfer

Token.Transfer(_from, _to, _value) `ddf252ad`

Arguments

| | | |
|-|-|-|
| *address* | _from | indexed |
| *address* | _to | indexed |
| *uint256* | _value | not indexed |

## *event* Approval

Token.Approval(_owner, _spender, _value) `8c5be1e5`

Arguments

| | | |
|-|-|-|
| *address* | _owner | indexed |
| *address* | _spender | indexed |
| *uint256* | _value | not indexed |

## *event* Burn

Token.Burn(_burner, _value) `cc16f5db`

Arguments

| | | |
|-|-|-|
| *address* | _burner | indexed |
| *uint256* | _value | not indexed |


---