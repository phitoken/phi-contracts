### PHI Token

Contains ERC223 Token and tests.

Token data:

Name: **PHI Token**

Symbol: **PHI**

Decimals: **18**

To note:

 * Fixed total supply
 * Burn function
 * On deploy all tokens will be transfered to: pre-ico contract, ico contract, wallet address and hardcoded pre-sale investors (check `contracts/PhiToken.sol` for the balances)
 * Added `lockTime` to lock all tokens (except for pre-ico and ico) based on UNIX timestamp
 * Emergency transfer function (team wallet can change balances only during locktime)

#### Requirments

 * Node version 7.10 or above

 * Debian

#### Install

cd into the token folder and run `npm install`

Run the ethereum test client provided (**only if you want to deploy/test contracts**):

`npm run testrpc`

Compile contracts and run migrations with truffle:

`npm run setup`

This will compile smart contracts, generates ABI and deploy it on the default network.

#### Tests

**Please note** Before running tests you need to follow "Install" above

Before running a test you need to have a ethereum client running, testrpc is included, see "Install".

Run: `node_modules/.bin/truffle test/PhiToken_test.js`


#### Docs

Docs for PhiToken is available [here](docs/PhiToken.md)

You can update it by running `npm run docs:build`

** Please note ** `solc` is required in your environment

