### ICO

Contains ICO contract and tests.

To note:

 * Allow only addresses
 * TOKEN -> USD Fixed pricing
 * Oraclize integration
 * Capped sale
 * KYC whitelist after the pre-ico ends
 * Burn unsold tokens

After deploy this steps are required:

 * call `initializeCrowdsale` **with some ETHs**

   * Setup start and end time, add PHI token address reference, start oraclize_query rate updates (when close to the crowdsale start time)

 * **On start time** call `changeStage(Stage.Running)` to start the crowdsale

   * This is not mandatory, the Running stage will be automatically set by the first investor

If the contract is running out of funds or Oraclize needs more funds send some with `topUpBalance(28391728448)`

The next steps are:

 * **On end time** call `changeStage(Stage.Finished)` to end the crowdsale 

  * This is not mandatory, the Finished stage will be automatically set by Oraclize during the normal ETH/USD rate updates (however the Finished stage can have an offset of `intervalUpdate`)

 * When you have the whitelisted address list from Eidoo

   * Check if balances matches with the provided list
   * If everything is fine, call `addWhitelistAddrByList` to enable these addresses or `addWhitelistAddress`
   * Check if whitelisted addresses and balances match with the onchain contract state by calling `getInvestorBalance(address)`
   * If everything is fine, call `finalizeInvestorsByList` or `finalizeSingleInvestor` and pass addresses that you want to finalize
   * Make sure every investors have the tokens on PHI token contract
   * If everything is fine, call `changeStage(Stage.Finalized)` to change the state
   * After that withdraw the remainging funds in the contract (since the crowdsale is ended we don't need oraclize_query) by calling `withdrawBalance()`, funds will be sent to the wallet address specified on deploy

#### Requirments

 * Node version 7.10 or above

 * Debian

#### Install

cd into the token folder and run `npm install`

Run the ethereum test client provided (**only if you want to deploy/test contracts**):

`npm run testrpc`

Compile contracts and run migrations with truffle:

`npm run setup`

This will compile smart contracts, generates ABI and deploy it on the default network.

Install ethereum-bridge with `npm install` into the folder

#### Tests

**Please note** Before running tests you need to follow "Install" above

Before running a test you need to have an ethereum client running, testrpc is included, see "Install".

Run ethereum-bridge with `node bridge --dev`

Run: `node_modules/.bin/truffle test/Ico_test.js`


#### Docs

Docs for Ico is available [here](docs/Ico.md)

You can update it by running `npm run docs`

** Please note ** `solc` is required in your environment

